# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.0 - 2020-08-24(07:02:53 +0000)

### New

- Test

## Release 0.0.1 - 2020-08-24(07:02:53 +0000)

### New

- Device object
- Config object
- Config RPCs: create, get, set, load, save
- Devices RPCs: find, createDevice, setLink
- Device tree traversal

## Release 0.0.0 - 2020-06-24(14:20:00 +0000)

### New

- Initial release
