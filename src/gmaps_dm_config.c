/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>

#include "gmaps_priv.h"


amxd_status_t _Config_create(amxd_object_t* object,
                             GMAPS_UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;

    char* module = amxc_var_dyncast(cstring_t, GET_ARG(args, "module"));

    status = gmaps_new_config(object, module);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    free(module);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Config_set(amxd_object_t* object,
                          GMAPS_UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          GMAPS_UNUSED amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;

    char* module = amxc_var_dyncast(cstring_t, GET_ARG(args, "module"));
    char* option = amxc_var_dyncast(cstring_t, GET_ARG(args, "option"));
    amxc_var_t* value = GET_ARG(args, "value");

    status = gmaps_set_config(object, module, option, value);

    amxc_var_clean(ret);

    free(module);
    free(option);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Config_get(amxd_object_t* object,
                          GMAPS_UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;

    char* module = amxc_var_dyncast(cstring_t, GET_ARG(args, "module"));
    char* option = amxc_var_dyncast(cstring_t, GET_ARG(args, "option"));

    status = gmaps_get_config(object, module, option, ret);

    free(module);
    free(option);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Config_save(amxd_object_t* object,
                           GMAPS_UNUSED amxd_function_t* func,
                           amxc_var_t* args,
                           amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;

    char* module = amxc_var_dyncast(cstring_t, GET_ARG(args, "module"));

    status = gmaps_save_config(object, module);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    free(module);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Config_load(amxd_object_t* object,
                           GMAPS_UNUSED amxd_function_t* func,
                           amxc_var_t* args,
                           amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;

    char* module = amxc_var_dyncast(cstring_t, GET_ARG(args, "module"));

    status = gmaps_load_config(object, module);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    free(module);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}


amxd_status_t _Config_scanMibDir(GMAPS_UNUSED amxd_object_t* object,
                                 GMAPS_UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    amxc_var_t* path = NULL;

    amxc_var_t dirs;
    amxc_var_init(&dirs);
    amxc_var_set_type(&dirs, AMXC_VAR_ID_LIST);

    path = amxc_var_add_new(&dirs);

    amxc_var_set_type(path, AMXC_VAR_ID_CSTRING);

    amxc_var_set(cstring_t, path, amxc_var_constcast(cstring_t, GET_ARG(args, "path")));

    status = amxo_parser_scan_mib_dirs(gmap_get_parser(), &dirs);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    amxc_var_clean(&dirs);

    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}