/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>

#include "gmaps_priv.h"


amxd_status_t _Devices_createDevice(amxd_object_t* object,
                                    GMAPS_UNUSED amxd_function_t* func,
                                    amxc_var_t* args,
                                    amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    amxd_object_t* device_templ = NULL;

    char* key = amxc_var_dyncast(cstring_t, GET_ARG(args, "key"));
    char* discovery_source =
        amxc_var_dyncast(cstring_t, GET_ARG(args, "discovery_source"));
    char* tags = amxc_var_dyncast(cstring_t, GET_ARG(args, "tags"));
    bool persistent = amxc_var_dyncast(bool, GET_ARG(args, "persistent"));
    char* default_name =
        amxc_var_dyncast(cstring_t, GET_ARG(args, "default_name"));
    amxc_var_t* values = GET_ARG(args, "values");

    device_templ = amxd_object_get_child(object, "Device");

    status = gmaps_new_device(device_templ,
                              key,
                              discovery_source,
                              tags,
                              persistent,
                              default_name,
                              values);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    free(key);
    free(discovery_source);
    free(tags);
    free(default_name);
    amxc_var_delete(&values);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Devices_destroyDevice(amxd_object_t* object,
                                     GMAPS_UNUSED amxd_function_t* func,
                                     amxc_var_t* args,
                                     amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    amxd_object_t* device_templ = NULL;

    char* key = amxc_var_dyncast(cstring_t, GET_ARG(args, "key"));

    device_templ = amxd_object_get_child(object, "Device");
    status = gmaps_delete_device(device_templ, key);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    free(key);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Devices_find(amxd_object_t* object,
                            GMAPS_UNUSED amxd_function_t* func,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    uint32_t flags = 0;
    char* expression = NULL;
    amxc_var_t* expression_var = GET_ARG(args, "expression");
    char* flags_char = amxc_var_dyncast(cstring_t, GET_ARG(args, "flags"));

    flags = gmap_devices_flags_from_cstring(flags_char);

    if(amxc_var_type_of(expression_var) == AMXC_VAR_ID_HTABLE) {
        // return a hash table
        amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
        // fetch the devices per expression available
        amxc_htable_for_each(it, amxc_var_constcast(amxc_htable_t, expression_var)) {
            // (key, value) of htable is (expression_id, expression)
            const char* expression_id = amxc_htable_it_get_key(it);
            expression = amxc_var_dyncast(cstring_t, amxc_var_from_htable_it(it));
            // add an empty list of devices
            amxc_var_t* devices = amxc_var_add_key(amxc_llist_t, ret, expression_id, NULL);
            // fetch devices
            status = gmaps_find_devices(object, expression, 0, flags, devices);
            when_failed(status, exit, amxc_var_clean(ret));
            free(expression);
            expression = NULL;
        }
    } else {
        // return a list
        amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
        // fetch devices
        // TODO: add support for UID
        expression = amxc_var_dyncast(cstring_t, expression_var);
        status = gmaps_find_devices(object, expression, 0, flags, ret);
        when_failed(status, exit, amxc_var_clean(ret));
    }

exit:
    free(expression);
    free(flags_char);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Devices_link(GMAPS_UNUSED amxd_object_t* object,
                            GMAPS_UNUSED amxd_function_t* func,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    char* upper_device = amxc_var_dyncast(cstring_t, GET_ARG(args, "upper_device"));
    char* lower_device = amxc_var_dyncast(cstring_t, GET_ARG(args, "lower_device"));
    char* type = amxc_var_dyncast(cstring_t, GET_ARG(args, "type"));

    char* upper_device_valid = gmap_create_valid_object_name(upper_device);
    char* lower_device_valid = gmap_create_valid_object_name(lower_device);

    status = gmaps_device_make_link(upper_device_valid,
                                    lower_device_valid,
                                    (type == NULL) ? "default" : type,
                                    true);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    free(upper_device);
    free(lower_device);
    free(upper_device_valid);
    free(lower_device_valid);
    free(type);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}
amxd_status_t _Devices_unlink(GMAPS_UNUSED amxd_object_t* object,
                              GMAPS_UNUSED amxd_function_t* func,
                              amxc_var_t* args,
                              amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    char* upper_device = amxc_var_dyncast(cstring_t, GET_ARG(args, "upper_device"));
    char* lower_device = amxc_var_dyncast(cstring_t, GET_ARG(args, "lower_device"));


    amxd_object_t* upper = gmaps_get_device(upper_device);
    amxd_object_t* lower = gmaps_get_device(lower_device);

    gmaps_device_removeLLink(upper, lower_device);
    gmaps_device_removeULink(lower, upper_device);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    free(upper_device);
    free(lower_device);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Devices_setLink(GMAPS_UNUSED amxd_object_t* object,
                               GMAPS_UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    char* upper_device = amxc_var_dyncast(cstring_t, GET_ARG(args, "upper_device"));
    char* lower_device = amxc_var_dyncast(cstring_t, GET_ARG(args, "lower_device"));
    char* type = amxc_var_dyncast(cstring_t, GET_ARG(args, "type"));

    status = gmaps_device_make_link(upper_device,
                                    lower_device,
                                    (type == NULL) ? "default" : type,
                                    false);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    free(upper_device);
    free(lower_device);
    free(type);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Devices_block(amxd_object_t* object,
                             GMAPS_UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             GMAPS_UNUSED amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    char* key = amxc_var_dyncast(cstring_t, GET_ARG(args, "key"));

    status = gmaps_block_device(object, key);

    free(key);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Devices_unblock(GMAPS_UNUSED amxd_object_t* object,
                               GMAPS_UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               GMAPS_UNUSED amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    char* key = amxc_var_dyncast(cstring_t, GET_ARG(args, "key"));

    status = gmaps_unblock_device(key);

    free(key);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Devices_isBlocked(GMAPS_UNUSED amxd_object_t* object,
                                 GMAPS_UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    bool blocked = false;
    char* key = amxc_var_dyncast(cstring_t, GET_ARG(args, "key"));

    blocked = gmaps_device_is_blocked(key);

    amxc_var_set(bool, ret, blocked);

    free(key);
    return amxd_status_ok;
}

amxd_status_t _Devices_get(GMAPS_UNUSED amxd_object_t* object,
                           GMAPS_UNUSED amxd_function_t* func,
                           amxc_var_t* args,
                           amxc_var_t* ret) {
    amxc_var_t* result = NULL;
    uint32_t flags = 0;
    char* expression = amxc_var_dyncast(cstring_t, GET_ARG(args, "expression"));
    char* flag_string = amxc_var_dyncast(cstring_t, GET_ARG(args, "flags"));

    flags = gmap_devices_flags_from_cstring(flag_string);

    result = gmaps_devices_get(expression, flags);

    amxc_var_copy(ret, result);

    free(expression);
    free(flag_string);
    amxc_var_delete(&result);

    return amxd_status_ok;
}