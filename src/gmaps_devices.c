/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include "gmaps_priv.h"

#include <amxd/amxd_object_event.h>

#define LLTD_PROBE_START "00:0D:3A:D7:F1:40"
#define LLTD_PROBE_END   "00:0D:3A:FF:FF:FF"

typedef struct _gmap_device {
    amxc_htable_it_t it;
    amxd_object_t* device;
} gmap_device_t;

typedef struct _gmap_matching_devs_info {
    amxc_var_t* matching_devs;
    amxp_expr_t* expression;
} gmap_matching_devs_info_t;

static bool gmaps_can_add_lan_device(GMAPS_UNUSED const char* tags) {
    // TODO: check if it is a lan device and can be added
    return true;
}

static bool gmaps_delete_oldest_lan_device(void) {
    // TODO: search oldest lan device and remove it
    return false;
}

static void gmaps_store_device(amxd_object_t* device) {
    gmap_device_t* dev = calloc(1, sizeof(gmap_device_t));
    dev->device = device;
    amxc_htable_insert(gmap_get_table(),
                       amxd_object_get_name(device, AMXD_OBJECT_NAMED),
                       &dev->it);
}

static amxd_object_t* gmaps_remove_device(const char* key) {
    amxd_object_t* retval = NULL;
    gmap_device_t* dev = NULL;
    amxc_htable_it_t* it = amxc_htable_take(gmap_get_table(), key);
    when_null(it, exit, retval = NULL);
    dev = amxc_htable_it_get_data(it, gmap_device_t, it);

    retval = dev->device;
    amxc_htable_it_clean(it, gmaps_clear_device);

exit:
    return retval;
}

/**
 * Utility function to add the @param device to a list of matching devices if it matches
 * the given expression in @param info.
 */
static gmap_status_t gmaps_add_matching_device(amxd_object_t* device,
                                               void* priv) {
    gmap_status_t status = gmap_status_ok;
    const gmap_matching_devs_info_t* info = NULL;

    when_null(priv, exit, status = gmap_status_invalid_parameter);

    info = (const gmap_matching_devs_info_t*) priv;

    if(gmaps_device_matches(device, info->expression)) {
        amxc_var_add(cstring_t,
                     info->matching_devs,
                     amxd_object_get_name(device, AMXD_OBJECT_NAMED));
    }

exit:
    return status;
}

/**
 * Returns true if the device with the given @param key is a LLTD probe.
 * LLTD probes should be considered as blocked devices.
 * Bugzilla reference: 78491.
 */
static bool gmaps_devices_is_LLTD_probe(const char* key) {
    if((strcmp(key, LLTD_PROBE_START) >= 0) && (strcmp(key, LLTD_PROBE_END) <= 0)) {
        return true;
    }
    return false;
}

amxd_object_t* gmaps_get_device(const char* key) {
    amxd_object_t* retval = NULL;
    gmap_device_t* dev = NULL;
    amxc_htable_it_t* it = amxc_htable_get(gmap_get_table(), key);
    when_null(it, exit, retval = NULL);
    dev = amxc_htable_it_get_data(it, gmap_device_t, it);

    retval = dev->device;

exit:
    return retval;
}

void gmaps_clear_device(GMAPS_UNUSED const char* key,
                        amxc_htable_it_t* it) {
    gmap_device_t* dev = amxc_htable_it_get_data(it, gmap_device_t, it);
    free(dev);
}

static void gmaps_new_add_parameters(amxd_object_t* new_dev, amxc_var_t* parameters) {
    amxc_htable_t* paramtable = amxc_var_get_amxc_htable_t(parameters);
    amxc_htable_for_each(it, paramtable) {
        amxc_var_t* data = amxc_htable_it_get_data(it, amxc_var_t, hit);
        const char* tkey = amxc_htable_it_get_key(it);

        if((strcmp(tkey, "Active") == 0) ||
           ( strcmp(tkey, "Name") == 0) ||
           ( strcmp(tkey, "Type") == 0) ||
           ( strcmp(tkey, "Key") == 0)) {
            continue;
        }

        amxd_object_set_param(new_dev, tkey, data);
        amxc_var_delete(&data);
    }
    amxc_htable_delete(&paramtable, 0);
}

gmap_status_t gmaps_new_device(amxd_object_t* devices,
                               const char* key,
                               const char* discovery_source,
                               const ssv_string_t tags,
                               bool persistent,
                               const char* default_name,
                               amxc_var_t* parameters) {
    amxc_ts_t now;
    gmap_status_t status = gmap_status_unknown_error;
    amxd_object_t* new_dev = NULL;
    char* name = NULL;

    when_null(devices, exit, status = gmap_status_invalid_parameter);
    when_str_empty(key, exit, status = gmap_status_invalid_key);
    when_true(gmaps_device_is_blocked(key), exit, status = gmap_status_invalid_key);
    when_true(!gmaps_can_add_lan_device(tags) &&
              !gmaps_delete_oldest_lan_device(),
              exit,
              status = gmap_status_device_limit_reached);

    name = gmap_create_valid_object_name(key);

    new_dev = gmaps_get_device(key);

    if(new_dev == NULL) {
        when_failed(amxd_object_new_instance(&new_dev, devices, name, 0, NULL),
                    exit,
                    status = gmap_status_unknown_error);
        gmaps_store_device(new_dev);
    }

    free(name);

    amxc_ts_now(&now);
    amxd_object_set_attr(new_dev, amxd_oattr_persistent, persistent);
    amxd_object_set_value(bool, new_dev, "Active", true);
    amxd_object_set_value(cstring_t, new_dev, "Key", key);
    amxd_object_set_value(cstring_t, new_dev, "DiscoverySource", discovery_source);
    // TODO: check ntp synchronization
    amxd_object_set_value(amxc_ts_t, new_dev, "FirstSeen", &now);
    amxd_object_set_value(amxc_ts_t, new_dev, "LastConnection", &now);

    gmaps_device_traverse_set_tag(new_dev, tags);
    amxo_parser_apply_mibs(gmap_get_parser(), new_dev, gmaps_device_matches);

    if((default_name != NULL) && (*default_name != 0)) {
        // TODO: set default name
    } else {
        // TODO: generate default name
    }

    // TODO: select name


    gmaps_new_add_parameters(new_dev, parameters);

    amxd_object_emit_add_inst(new_dev);

    // TODO: merge rules?

    status = gmap_status_ok;
exit:
    return status;
}

gmap_status_t gmaps_delete_device(amxd_object_t* devices, const char* key) {
    amxd_status_t status = gmap_status_ok;
    amxd_object_t* dev = NULL;

    when_str_empty(key, exit, status = gmap_status_invalid_key);
    when_null(devices, exit, status = gmap_status_invalid_parameter);

    dev = gmaps_remove_device(key);
    when_null(dev, exit, status = gmap_status_ok);

    // TODO: check if it has tag self, can not be deleted
    // TODO: Remove all ULinks & LLinks
    // TODO: Clean-up alternatives
    // TODO: Remove all names from hash table

    amxd_object_emit_del_inst(dev);

    amxd_object_delete(&dev); // this should not fail


    // TODO: Delete merge rules
exit:
    return status;
}

gmap_status_t gmaps_block_device(amxd_object_t* devices, const char* key) {
    amxd_status_t status = gmap_status_unknown_error;
    amxc_htable_it_t* hit = NULL;

    when_null(devices, exit, status = gmap_status_invalid_parameter);
    when_str_empty(key, exit, status = gmap_status_invalid_key);

    // Check if already blocked.
    when_not_null(amxc_htable_get(gmap_get_blocked_devices(), key),
                  exit,
                  status = gmap_status_ok);

    hit = calloc(1, sizeof(amxc_htable_it_t));
    amxc_htable_it_init(hit);
    amxc_htable_insert(gmap_get_blocked_devices(), key, hit);

    // If the blocked device exists, remove it.
    gmaps_delete_device(devices, key);

    status = gmap_status_ok;
exit:
    return status;
}

gmap_status_t gmaps_unblock_device(const char* key) {
    amxd_status_t status = gmap_status_unknown_error;
    amxc_htable_it_t* hit = NULL;

    when_str_empty(key, exit, status = gmap_status_invalid_key);

    hit = amxc_htable_take(gmap_get_blocked_devices(), key);

    // Check if already unblocked.
    when_null(hit,
              exit,
              status = gmap_status_ok);

    amxc_htable_it_clean(hit, NULL);
    free(hit);

    status = gmap_status_ok;
exit:
    return status;
}

bool gmaps_device_is_blocked(const char* key) {
    bool retval = false;

    when_str_empty(key, exit, retval = false);

    when_true(gmaps_devices_is_LLTD_probe(key),
              exit,
              retval = true);

    when_not_null(amxc_htable_get(gmap_get_blocked_devices(), key),
                  exit,
                  retval = true);

exit:
    return retval;
}

gmap_status_t gmaps_find_devices(amxd_object_t* devices,
                                 const char* expression,
                                 uint32_t UID,
                                 uint32_t flags,
                                 amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    amxp_expr_t* expr = NULL;
    amxd_object_t* device_template = NULL;

    when_failed(amxp_expr_new(&expr, expression),
                exit,
                status = gmap_status_invalid_expression);

    when_null(devices, exit, status = gmap_status_invalid_parameter);
    when_null(ret, exit, status = gmap_status_invalid_parameter);

    device_template = amxd_object_findf(devices, "Device");
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);

    amxd_object_for_each(instance, it, device_template) {
        amxd_object_t* device = amxc_llist_it_get_data(it, amxd_object_t, it);

        //TODO add acl check (object_canRead(device, UID)) when available
        (void) UID;

        if(((flags & GMAP_INCLUDE_ALTERNATIVES) == 0) && gmaps_device_is_alternative(device)) {
            // skip alternative devices
            continue;
        }

        // check that the (master) device matches with the expression
        if(gmaps_device_matches(device, expr)) {
            // match found, add to the result list
            amxc_var_add(cstring_t,
                         ret,
                         amxd_object_get_name(device, AMXD_OBJECT_NAMED));
            // continue with next device
            continue;
        }

        // check alternatives if GMAP_INCLUDE_ALTERNATIVES flag is not set
        if((flags & GMAP_INCLUDE_ALTERNATIVES) == 0) {
            gmap_matching_devs_info_t match_info = {.matching_devs = ret, .expression = expr};
            gmaps_device_for_all_alternatives(device, gmaps_add_matching_device, &match_info);
        }
    }

exit:
    amxp_expr_delete(&expr);
    return status;
}

/**
 * The master device of @param device is returned via the parameter if it exists.
 * @return True if success, false is failed.
 */
bool gmaps_device_get_master(amxd_object_t** device) {
    bool retval = false;
    char* master = amxd_object_get_value(cstring_t, *device, "Master", NULL);
    amxd_object_t* master_device = NULL;

    when_str_empty(master, exit, retval = true);
    master_device = gmaps_get_device(master);
    when_null(master_device, exit, retval = false);
    *device = master_device;
    retval = true;
exit:
    free(master);
    return retval;
}

/**
 * @brief Get a list of devices matching expression
 *
 * Using an expression this function is searching the data model for device that are matching the expression.
 *
 * A variant list containing the detailed information of the devices is build and returned
 *
 * A master device will match the expression if the device itself matchs or if any of its alternatives are matching.
 *
 * If the @ref GMAP_INCLUDE_ALTERNATIVES flag is specified, a master will only match the provided expression if the master
 *   device itself matches. The alternative devices are added to the returned list if thay are matching the expression.
 *
 * The details of the returned information can be changed by providing the flags bitmask. Supported flags are:
 *
 * @ref GMAP_NO_DETAILS: no parameters are added in the returned result
 * @ref GMAP_NO_ACTIONS: no actions are added in the returned result
 * @ref GMAP_INCLUDE_TOPOLOGY: The topology (down direction) for each device is added in the returned result.
 * @ref GMAP_INCLUE_ALTERNATIVES: match alternative devices individually
 */
amxc_var_t* gmaps_devices_get(const char* expression, uint32_t flags) {
    amxc_var_t* devlist = NULL;
    amxc_var_t* retv = NULL;
    amxd_object_t* devroot = NULL;

    amxc_var_new(&devlist);
    amxc_var_set_type(devlist, AMXC_VAR_ID_LIST);

    devroot = amxd_dm_findf(gmap_get_dm(), "Devices");

    amxc_var_new(&retv);
    amxc_var_set_type(retv, AMXC_VAR_ID_LIST);

    gmaps_find_devices(devroot, expression, 0, flags, devlist);

    amxc_var_for_each(device, devlist) {
        const char* key = amxc_var_constcast(cstring_t, device);
        amxc_var_t* newinfo = NULL;

        newinfo = amxc_var_add_new(retv);
        amxc_var_set_type(newinfo, AMXC_VAR_ID_HTABLE);

        gmaps_device_get(key, newinfo, flags);
    }


    amxc_var_delete(&devlist);

    return retv;
}