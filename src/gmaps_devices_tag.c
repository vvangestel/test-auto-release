/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include "gmaps_priv.h"


static bool gmaps_device_update_tags(amxd_object_t* device,
                                     amxc_var_t* tags) {
    bool retval = true;
    amxd_trans_t* transaction = NULL;

    amxd_trans_new(&transaction);

    amxd_trans_select_object(transaction, device);
    amxd_trans_set_attr(transaction, amxd_tattr_change_ro, true);
    amxd_trans_set_param(transaction, "Tags", tags);
    when_failed(amxd_trans_apply(transaction, gmap_get_dm()),
                exit,
                retval = false);

exit:
    amxd_trans_delete(&transaction);
    return retval;
}

/**
 * Adds single tag to tags if tag is not in tags.
 * The parameter tags should be a variant of type AMXC_VAR_ID_SSV_STRING.
 */
static void gmaps_add_tag(amxc_var_t* tags,
                          amxc_string_t* tag) {
    amxc_llist_t* tags_list = NULL;
    amxc_string_t* tags_str = NULL;

    when_true(amxc_string_is_empty(tag), exit, );  // nothing to do

    tags_list = amxc_var_dyncast(amxc_llist_t, tags);
    amxc_llist_for_each(it, tags_list) {
        amxc_var_t* v = amxc_var_from_llist_it(it);
        if(strcmp(amxc_var_constcast(cstring_t, v), amxc_string_get(tag, 0)) == 0) {
            goto exit;
        }
    }

    tags_str = amxc_var_take(amxc_string_t, tags);
    if(tags_str == NULL) {
        amxc_string_new(&tags_str, amxc_string_text_length(tag) + 2);
    }
    amxc_string_appendf(tags_str, " %s", amxc_string_get(tag, 0));
    amxc_string_trim(tags_str, NULL);
    amxc_var_push(ssv_string_t, tags, amxc_string_take_buffer(tags_str));

exit:
    amxc_llist_delete(&tags_list, variant_list_it_free);
    amxc_string_delete(&tags_str);
    return;
}



/**
 * Clears a tag from tags if parameter is there
 */
static void gmaps_clear_tag(amxc_var_t* tags,
                            amxc_string_t* tag) {

    amxc_llist_t* tags_list = NULL;
    amxc_var_t tag_result;

    amxc_var_init(&tag_result);
    amxc_var_set_type(&tag_result, AMXC_VAR_ID_SSV_STRING);

    when_true(amxc_string_is_empty(tag), exit, );  // nothing to do

    tags_list = amxc_var_dyncast(amxc_llist_t, tags);
    amxc_llist_for_each(it, tags_list) {
        amxc_var_t* v = amxc_var_from_llist_it(it);
        if(strcmp(amxc_var_constcast(cstring_t, v), amxc_string_get(tag, 0)) != 0) {
            amxc_string_t ctag;
            const char* str = amxc_var_constcast(cstring_t, v);
            amxc_string_init(&ctag, 0);
            amxc_string_appendf(&ctag, "%s", str);
            gmaps_add_tag(&tag_result, &ctag);

            amxc_string_clean(&ctag);
        }

    }

    amxc_var_copy(tags, &tag_result);
    amxc_var_clean(&tag_result);

exit:
    amxc_llist_delete(&tags_list, variant_list_it_free);
    return;

}

bool gmaps_device_traverse_has_tag(amxd_object_t* device,
                                   const char* tag) {
    bool retval = false;
    amxc_var_t* device_tags = NULL;
    amxc_llist_t* tags_list = NULL;
    char* tags_cstring = NULL;
    amxc_string_t* tags_str = NULL;

    amxc_var_new(&device_tags);
    amxc_llist_new(&tags_list);
    amxc_string_new(&tags_str, 0);

    when_failed(amxd_object_get_param(device, "Tags", device_tags),
                exit,
                retval = false);

    tags_cstring = amxc_var_dyncast(cstring_t, device_tags);
    when_str_empty(tags_cstring, exit, retval = false);
    amxc_string_push_buffer(tags_str, tags_cstring, strlen(tags_cstring) + 1);
    tags_cstring = NULL;
    amxc_string_split_to_llist(tags_str, tags_list, ' ');

    amxc_llist_for_each(it, tags_list) {
        amxc_string_t* s = amxc_string_from_llist_it(it);
        const char* txt_part = amxc_string_get(s, 0);
        if(strcmp(txt_part, tag) == 0) {
            retval = true;
            break;
        }
    }

exit:
    amxc_var_delete(&device_tags);
    amxc_llist_delete(&tags_list, amxc_string_list_it_free);
    amxc_string_delete(&tags_str);
    return retval;
}

/**
 * Add tags to the Tags parameter of the given device object.
 */
bool gmaps_device_traverse_set_tag(amxd_object_t* device,
                                   const ssv_string_t tags) {
    bool retval = true;
    amxc_var_t* device_tags = NULL;
    amxc_string_t* tags_string = NULL;
    amxc_llist_t* tags_list = NULL;

    amxc_var_new(&device_tags);
    amxc_string_new(&tags_string, 0);
    amxc_llist_new(&tags_list);

    when_str_empty(tags,
                   exit,
                   retval = true); // nothing to do

    amxc_string_appendf(tags_string, "%s", tags);
    when_failed(amxd_object_get_param(device, "Tags", device_tags),
                exit,
                retval = false);
    amxc_string_split_to_llist(tags_string, tags_list, ' ');

    amxc_llist_for_each(it, tags_list) {
        amxc_string_t* single_tag = amxc_string_from_llist_it(it);
        gmaps_add_tag(device_tags, single_tag);
    }

    when_failed(gmaps_device_update_tags(device, device_tags),
                exit,
                retval = false);

exit:
    amxc_var_delete(&device_tags);
    amxc_string_delete(&tags_string);
    amxc_llist_delete(&tags_list, amxc_string_list_it_free);
    return retval;
}

bool gmaps_device_traverse_clear_tag(amxd_object_t* device,
                                     const ssv_string_t tags) {
    bool retval = true;
    amxc_var_t* device_tags = NULL;
    amxc_string_t* tags_string = NULL;
    amxc_llist_t* tags_list = NULL;

    amxc_var_new(&device_tags);
    amxc_string_new(&tags_string, 0);
    amxc_llist_new(&tags_list);

    when_str_empty(tags,
                   exit,
                   retval = true); // nothing to do

    amxc_string_appendf(tags_string, "%s", tags);
    when_failed(amxd_object_get_param(device, "Tags", device_tags),
                exit,
                retval = false);
    amxc_string_split_to_llist(tags_string, tags_list, ' ');

    amxc_llist_for_each(it, tags_list) {
        amxc_string_t* single_tag = amxc_string_from_llist_it(it);
        gmaps_clear_tag(device_tags, single_tag);
    }

    amxc_var_dump(device_tags, 0);

    when_failed(gmaps_device_update_tags(device, device_tags),
                exit,
                retval = false);

exit:
    amxc_var_delete(&device_tags);
    amxc_string_delete(&tags_string);
    amxc_llist_delete(&tags_list, amxc_string_list_it_free);
    return retval;
}

