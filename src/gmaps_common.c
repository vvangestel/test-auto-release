/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>

#include "gmaps_priv.h"


typedef struct _gmap_server {
    amxd_dm_t* gmap_dm;             /** the gMap data model object */
    amxo_parser_t* parser;          /** the odl parser */
    amxc_htable_t devices;          /**< k,v = key string, instance object pointer */
    amxc_htable_t blocked_devices;  /**< hash set of device keys that are blocked */
} gmap_server_t;

static gmap_server_t server;
static bool ntp_synchronized = false;

static void gmap_server_set_config_if_unset(const char* name,
                                            const char* value) {
    amxc_var_t* option = amxo_parser_get_config(gmap_get_parser(), name);

    if(amxc_var_is_null(option)) {
        amxc_var_t value_var;
        amxc_var_init(&value_var);
        amxc_var_set(cstring_t, &value_var, value);
        amxo_parser_set_config(gmap_get_parser(), name, &value_var);
        amxc_var_clean(&value_var);
    }
}

static void gmap_server_populate_config(void) {
    gmap_server_set_config_if_unset("gmap_save_path", "/tmp/gmap");
    gmap_server_set_config_if_unset("gmap_defaults_path", "/usr/lib/gmap/config");
}

static void gmap_server_create_savedir(void) {
    amxc_string_t* savedir = NULL;
    amxc_string_new(&savedir, 0);
    struct stat st;

    memset(&st, 0, sizeof(st));

    amxc_string_appendf(savedir, "${gmap_save_path}");
    amxc_string_resolve(savedir, gmap_get_config());

    if(stat(amxc_string_get(savedir, 0), &st) == -1) {
        mkdir(amxc_string_get(savedir, 0), S_IRWXU);
    }

    amxc_string_delete(&savedir);
}

static void gmaps_clear_blocked_device(GMAPS_UNUSED const char* key,
                                       amxc_htable_it_t* it) {
    free(it);
}

void gmap_server_init(amxd_dm_t* dm,
                      amxo_parser_t* parser) {
    amxc_htable_init(&server.devices, 10);
    amxc_htable_init(&server.blocked_devices, 10);
    server.gmap_dm = dm;
    server.parser = parser;

    gmap_server_populate_config();
    gmap_server_create_savedir();
    gmaps_device_name_table_init();
}

void gmap_server_cleanup(void) {
    amxc_htable_clean(&server.devices, gmaps_clear_device);
    amxc_htable_clean(&server.blocked_devices, gmaps_clear_blocked_device);
    gmaps_device_name_table_cleanup();
}

amxd_dm_t* gmap_get_dm(void) {
    return server.gmap_dm;
}

amxo_parser_t* gmap_get_parser(void) {
    return server.parser;
}

amxc_var_t* gmap_get_config(void) {
    return &(server.parser->config);
}

amxc_htable_t* gmap_get_table(void) {
    return &server.devices;
}

amxc_htable_t* gmap_get_blocked_devices(void) {
    return &server.blocked_devices;
}

bool gmap_is_ntp_synchronized(void) {
    // Stubbed for now.
    // TODO: implement NTP syncing.

    //return ntp_synchronized;
    (void) ntp_synchronized;
    return true;
}
