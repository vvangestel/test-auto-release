/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <stdlib.h>
#include <string.h>

#include "gmaps_priv.h"


typedef struct _gmaps_name_table_entry {
    amxc_htable_it_t hit;   // Iterator.
    amxc_llist_t indices;   // Linked list of indices in use for a name.
    uint32_t count;         // Count of indices in use.
} gmaps_name_table_entry_t;

typedef struct _gmaps_name_table_entry_list_item {
    amxc_llist_it_t lit;
    uint32_t index;
} gmaps_name_table_entry_list_item_t;


static amxc_htable_t gmaps_name_table;


static void gmaps_name_table_entry_list_item_new(gmaps_name_table_entry_list_item_t** item,
                                                 uint32_t index) {
    *item = calloc(1, sizeof(gmaps_name_table_entry_list_item_t));
    amxc_llist_it_init(&(*item)->lit);
    (*item)->index = index;
}

static void gmaps_name_table_entry_list_it_clean(amxc_llist_it_t* lit) {
    gmaps_name_table_entry_list_item_t* item = amxc_llist_it_get_data(lit,
                                                                      gmaps_name_table_entry_list_item_t,
                                                                      lit);
    free(item);
}

static void gmaps_name_table_entry_new(gmaps_name_table_entry_t** entry) {
    *entry = calloc(1, sizeof(gmaps_name_table_entry_t));
    amxc_htable_it_init(&(*entry)->hit);
    amxc_llist_init(&(*entry)->indices);
    (*entry)->count = 0;
}

static void gmaps_name_table_entry_delete(gmaps_name_table_entry_t** entry) {
    amxc_llist_clean(&(*entry)->indices, gmaps_name_table_entry_list_it_clean);
    free(*entry);
    *entry = NULL;
}

static void gmaps_name_table_entry_it_clean(GMAPS_UNUSED const char* key,
                                            amxc_htable_it_t* hit) {
    gmaps_name_table_entry_t* entry = amxc_htable_it_get_data(hit,
                                                              gmaps_name_table_entry_t,
                                                              hit);
    gmaps_name_table_entry_delete(&entry);
}

static bool gmaps_name_table_entry_contains_index(gmaps_name_table_entry_t* entry, uint32_t index) {
    amxc_llist_for_each(lit, &entry->indices) {
        gmaps_name_table_entry_list_item_t* item = amxc_llist_it_get_data(lit,
                                                                          gmaps_name_table_entry_list_item_t,
                                                                          lit);
        if(item->index == index) {
            return true;
        }
    }
    return false;
}


void gmaps_device_name_table_init(void) {
    amxc_htable_init(&gmaps_name_table, 300);
}

void gmaps_device_name_table_cleanup(void) {
    amxc_htable_clean(&gmaps_name_table, gmaps_name_table_entry_it_clean);
}

/**
 * Adds @param index to the linked list in the names table entry
 * at the key given by @param name.
 * If the entry does not exist, it is created.
 */
void gmaps_device_name_table_add(const char* name, uint32_t index) {
    amxc_htable_it_t* hit = amxc_htable_get(&gmaps_name_table, name);
    gmaps_name_table_entry_t* entry = NULL;
    gmaps_name_table_entry_list_item_t* indices_item = NULL;

    if(hit == NULL) {
        gmaps_name_table_entry_new(&entry);
        amxc_htable_insert(&gmaps_name_table, name, &entry->hit);
    } else {
        entry = amxc_htable_it_get_data(amxc_htable_get(&gmaps_name_table,
                                                        name),
                                        gmaps_name_table_entry_t,
                                        hit);
    }

    gmaps_name_table_entry_list_item_new(&indices_item, index);
    amxc_llist_append(&entry->indices, &indices_item->lit);
    entry->count++;
}

/**
 * Removes @param index from the linked list in the names table entry
 * at the key given by @param name.
 * If @param index appears multiple times in the linked list, only one instance is removed.
 * If the linked list is empty after this operation, the names table entry is removed.
 */
bool gmaps_device_name_table_remove(const char* const name, uint32_t index) {
    bool retval = false;
    amxc_htable_it_t* hit = amxc_htable_get(&gmaps_name_table, name);
    gmaps_name_table_entry_t* entry = NULL;

    when_null(hit, exit, retval = false);

    entry = amxc_htable_it_get_data(amxc_htable_get(&gmaps_name_table,
                                                    name),
                                    gmaps_name_table_entry_t,
                                    hit);

    amxc_llist_for_each(lit, &entry->indices) {
        gmaps_name_table_entry_list_item_t* item = amxc_llist_it_get_data(lit,
                                                                          gmaps_name_table_entry_list_item_t,
                                                                          lit);
        if(item->index == index) {
            amxc_llist_it_clean(lit, gmaps_name_table_entry_list_it_clean);
            entry->count--;
            retval = true;
            break;
        }
    }

    if(entry->count == 0) {
        amxc_htable_it_clean(hit, gmaps_name_table_entry_it_clean);
    }

exit:
    return retval;
}

uint32_t gmaps_device_name_table_get_first_available_index(const char* const name) {
    uint32_t index = 0;
    amxc_htable_it_t* hit = amxc_htable_get(&gmaps_name_table, name);
    gmaps_name_table_entry_t* entry = NULL;

    if(hit == NULL) {
        return 0;
    }
    entry = amxc_htable_it_get_data(hit, gmaps_name_table_entry_t, hit);

    for(index = 0; index < entry->count; ++index) {
        if(!gmaps_name_table_entry_contains_index(entry, index)) {
            return index;
        }
    }
    return entry->count;
}

bool gmaps_device_name_table_is_index_available(const char* const name, uint32_t index) {
    amxc_htable_it_t* hit = amxc_htable_get(&gmaps_name_table, name);
    gmaps_name_table_entry_t* entry = NULL;

    if(hit == NULL) {
        return true;
    }
    entry = amxc_htable_it_get_data(hit, gmaps_name_table_entry_t, hit);

    return !gmaps_name_table_entry_contains_index(entry, index);
}
