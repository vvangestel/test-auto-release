/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include "gmaps_priv.h"


/**
 * Custom field fetcher for gMap.
 * Called for each field (part of the expression) for which the expression
 * parser needs additional information.
 * For gMap, we look both at the object path and the tags.
 *
 * Example of an expression: ".Active == true and (wifi or eth)"
 * This expression has 3 fields for which the parser needs more info:
 *  1) .Active
 *  2) wifi
 *  3) eth
 *
 * The first one can be handled by the custom field fetcher from
 * the amxd lib, see @ref amxd_object_expr_get_field.
 * The second and third ones are tags and need an gMap-specific field
 * fetcher, see @ref gmaps_tag_field_fetcher.
 *
 * Note: gMap differentiates between tag and object path by looking at the first
 * character. If it's a dot, it is considered an object path. Else, a tag.
 */
static amxp_expr_status_t gmaps_custom_field_fetcher(amxp_expr_t* expr,
                                                     amxc_var_t* value,
                                                     const char* path,
                                                     void* priv) {
    amxp_expr_status_t status = amxp_expr_status_unknown_error;
    amxd_object_t* device = (amxd_object_t*) priv;

    if(path[0] == '.') {
        // Field is object path.
        status = amxd_object_expr_get_field(expr, value, path, device);
    } else {
        // Field is tag.
        amxc_var_set(bool, value, gmaps_device_traverse_has_tag(device, path));
        status = amxp_expr_status_ok;
    }

    return status;
}

bool gmaps_device_matches(amxd_object_t* device,
                          amxp_expr_t* expression) {
    return amxp_expr_evaluate(expression, gmaps_custom_field_fetcher, device, NULL);
}