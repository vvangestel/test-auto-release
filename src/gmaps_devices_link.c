/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include "gmaps_priv.h"


/**
 * Creates an upper or lower link for the given @param device, depending on @param link_type.
 * If any link of the given @param link_type already exists between the
 * devices, nothing is done and the exisiting link is returned.
 * @return The newly created link object or NULL if the link creation failed.
 */
static amxd_object_t* gmaps_device_create_link(amxd_object_t* device,
                                               const char* key,
                                               const char* type_parameter,
                                               gmaps_link_type_t link_type) {
    amxd_object_t* linked_template = amxd_object_findf(device,
                                                       "%s",
                                                       gmaps_device_get_link_text(link_type));
    amxd_object_t* link = amxd_object_findf(linked_template,
                                            "%s",
                                            key);
    amxd_trans_t trans;
    char* valid_key = gmap_create_valid_object_name(key);
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    if(link == NULL) {
        amxd_trans_select_object(&trans, linked_template);
        amxd_trans_add_inst(&trans, 0, key);
    } else {
        amxd_trans_select_object(&trans, link);
    }
    amxd_trans_set_value(cstring_t, &trans, "Type", type_parameter);
    amxd_trans_set_value(cstring_t, &trans, "Alias", valid_key);
    amxd_trans_apply(&trans, gmap_get_dm());

    amxd_trans_clean(&trans);

    free(valid_key);

    return amxd_object_findf(linked_template, "%s", key);
}

void gmaps_device_removeULink(amxd_object_t* device, const char* key) {
    amxd_object_t* ulink = amxd_object_findf(device, "UDevice.%s", key);
    amxd_object_t* ulink_templ = amxd_object_findf(device, "UDevice");
    when_null(ulink, exit, );
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_trans_select_object(&trans, ulink_templ);
    amxd_trans_del_inst(&trans, 0, key);
    amxd_trans_apply(&trans, gmap_get_dm());
    amxd_trans_clean(&trans);
exit:
    return;
}

void gmaps_device_removeLLink(amxd_object_t* device, const char* key) {
    amxd_object_t* llink = amxd_object_findf(device, "LDevice.%s", key);
    amxd_object_t* llink_templ = amxd_object_findf(device, "LDevice");
    when_null(llink, exit, );
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_trans_select_object(&trans, llink_templ);
    amxd_trans_del_inst(&trans, 0, key);
    amxd_trans_apply(&trans, gmap_get_dm());
    amxd_trans_clean(&trans);

exit:
    return;
}

void gmaps_device_removeAllULink(amxd_object_t* device, const char* keep) {
    amxd_object_t* link = amxd_object_findf(device, "UDevice");

    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_trans_select_object(&trans, link);

    amxd_object_for_each(instance, ulink_it, link) {
        amxd_object_t* ulink = amxc_llist_it_get_data(ulink_it, amxd_object_t, it);
        if(!keep || (strcmp(keep, amxd_object_get_name(ulink, 0)) != 0)) {
            const char* key = amxd_object_get_name(ulink, 0);
            amxd_trans_del_inst(&trans, 0, key);
        }
    }

    amxd_trans_apply(&trans, gmap_get_dm());
    amxd_trans_clean(&trans);
}


void gmaps_device_removeAllLLink(amxd_object_t* device, const char* keep) {
    amxd_object_t* link = amxd_object_findf(device, "LDevice");

    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_trans_select_object(&trans, link);

    amxd_object_for_each(instance, llink_it, link) {
        amxd_object_t* llink = amxc_llist_it_get_data(llink_it, amxd_object_t, it);
        if(!keep || (strcmp(keep, amxd_object_get_name(llink, 0)) != 0)) {
            const char* key = amxd_object_get_name(llink, 0);
            amxd_trans_del_inst(&trans, 0, key);
        }
    }

    amxd_trans_apply(&trans, gmap_get_dm());
    amxd_trans_clean(&trans);
}

/**
 * Device specific book keeping.
 * TODO: consider to provide a hook for this.
 */
static bool gmaps_device_update_interface_name(amxd_object_t* upper,
                                               amxd_object_t* lower) {
    bool retval = false;
    char* netdevname = amxd_object_get_value(cstring_t, upper, "NetDevName", NULL);
    char* name = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_str_empty(netdevname, exit, );

    name = amxd_object_get_value(cstring_t, upper, "Name", NULL);
    amxd_trans_select_object(&trans, lower);
    amxd_trans_set_value(cstring_t, &trans, "Layer2Interface", netdevname);
    amxd_trans_set_value(cstring_t, &trans, "InterfaceName", name);
    when_failed(amxd_trans_apply(&trans, gmap_get_dm()), exit, retval = false);

    retval = true;
exit:
    free(netdevname);
    free(name);
    amxd_trans_clean(&trans);
    return retval;
}

/**
 * Callback that checks if the @param device has a link in the direction
 * given in @param priv.
 *
 * @return Returns @ref gmap_status_break if the link is
 * found, else @param gmap_status_ok.
 */
static gmap_status_t gmaps_device_cb_check_link(amxd_object_t* device,
                                                void* priv) {
    return gmaps_device_has_link(device, (gmaps_link_type_t) priv, false)
           ? gmap_status_break
           : gmap_status_ok;
}

gmap_status_t gmaps_device_make_link(const char* upper,
                                     const char* lower,
                                     const char* type,
                                     bool single) {
    gmap_status_t status = gmap_status_unknown_error;
    amxd_object_t* upper_dev = gmaps_get_device(upper);
    amxd_object_t* lower_dev = gmaps_get_device(lower);

    when_null(upper_dev, exit, status = gmap_status_device_not_found);
    when_null(lower_dev, exit, status = gmap_status_device_not_found);
    when_null(type, exit, status = gmap_status_invalid_parameter);

    // A master and an alternative device can not be linked together.
    // They are already linked in a relationship. (master - alternative).
    when_true(gmaps_device_are_alternatives(upper_dev, lower_dev),
              exit,
              status = gmap_status_invalid_request);

    // Use master for linking if it exists.
    when_false(gmaps_device_get_master(&upper_dev),
               exit,
               status = gmap_status_internal_error);
    when_false(gmaps_device_get_master(&lower_dev),
               exit,
               status = gmap_status_internal_error);


    // IMPROVEMENT Consider transaction, so that all succeeds or all fails

    if(single) {
        gmaps_device_removeAllLLink(upper_dev, NULL);
        gmaps_device_removeAllULink(lower_dev, NULL);
    }

    // Check that the link already exists.
    when_true(gmaps_device_is_linked_to(lower_dev, upper_dev, gmap_traverse_one_up),
              exit,
              status = gmap_status_ok);

    // Create the links (from lower to upper and from upper to lower).
    when_null(gmaps_device_create_link(upper_dev, lower, type, gmaps_link_type_lower_link),
              exit,
              status = gmap_status_internal_error);
    when_null(gmaps_device_create_link(lower_dev, upper, type, gmaps_link_type_upper_link),
              exit,
              status = gmap_status_internal_error);

    // Change Layer2Interface and InterfaceName.
    // IMPROVEMENT consider to provide a hook for this.
    when_failed(gmaps_device_update_interface_name(upper_dev, lower_dev),
                exit,
                status = gmap_status_internal_error);

    status = gmap_status_ok;
exit:
    return status;
}


/* used for is linked to traverse */
typedef struct _linked_to_data {
    amxd_object_t* search_device;
    bool result;
} linked_to_data_t;

static gmap_traverse_status_t gmaps_traverse_is_linked_to(amxd_object_t* device,
                                                          gmap_traverse_action_t action,
                                                          void* userdata) {
    gmap_traverse_status_t status = gmap_traverse_continue;
    linked_to_data_t* data = userdata;

    switch(action) {
    case gmap_traverse_device_not_matching:
        break;
    case gmap_traverse_device_matching:

        if(strcmp(amxd_object_get_name(device, AMXD_OBJECT_NAMED), amxd_object_get_name(data->search_device, AMXD_OBJECT_NAMED)) == 0) {
            data->result = true;
            status = gmap_traverse_done;
        } else if(!data->result) {
            amxd_object_t* alternatives = NULL;
            amxd_object_t* altdevice = NULL;
            // if a device has alternatives and if one of the alternatives is active, the master device is
            // active as well.
            // loop over all alternatives until one of the alternatives is set to active
            alternatives = amxd_object_findf(device, "Alternative");
            amxd_object_for_each(instance, lit, alternatives) {
                altdevice = amxc_llist_it_get_data(lit, amxd_object_t, it);
                if(strcmp(amxd_object_get_name(data->search_device, AMXD_OBJECT_NAMED), amxd_object_get_name(altdevice, AMXD_OBJECT_NAMED)) == 0) {
                    data->result = true;
                    status = gmap_traverse_done;
                }


            }
        }
        break;
    case gmap_traverse_start:
        break;
    case gmap_traverse_level_push:
        break;
    case gmap_traverse_device_done:
        break;
    case gmap_traverse_level_pop:
        break;
    case gmap_traverse_stop:
        break;
    default:
        break;
    }

    return status;

}

bool gmaps_device_is_linked_to(amxd_object_t* dev,
                               amxd_object_t* device,
                               gmap_traverse_mode_t mode) {

    bool result = false;
    amxd_object_t* alternatives = NULL;
    amxd_object_t* altdevice = NULL;
    linked_to_data_t data;

    when_null(device, exit, );
    when_null(dev, exit, );

    // if the device is an alternative for a master device, check the links of the master device
    gmaps_device_get_master(&dev);

    // loop over the alternatives and check if one the alternatives is matching the device key
    if((mode == gmap_traverse_this) ||
       ( mode == gmap_traverse_down) ||
       ( mode == gmap_traverse_up) ||
       ( mode == gmap_traverse_one_down) ||
       ( mode == gmap_traverse_one_up)) {
        alternatives = amxd_object_findf(dev, "Alternative");
        amxd_object_for_each(instance, lit, alternatives) {
            altdevice = amxc_llist_it_get_data(lit, amxd_object_t, it);

            if(strcmp(amxd_object_get_name(device, AMXD_OBJECT_NAMED), amxd_object_get_name(altdevice, AMXD_OBJECT_NAMED)) == 0) {
                result = true;
            }

        }
    }

    if(!result) {
        data.result = false;
        data.search_device = device;
        gmaps_traverse_tree(dev, mode, "", gmaps_traverse_is_linked_to, &data);
        result = data.result;
    }

exit:
    return result;

}

/**
 * Returns true if the @param device has an edge of the given @param type.
 * If @param include_alternatives is true, also the alternative devices are considered.
 */
bool gmaps_device_has_link(amxd_object_t* device,
                           gmaps_link_type_t type,
                           bool include_alternatives) {
    bool retval = false;
    amxd_object_t* edge_template = amxd_object_findf(device,
                                                     "%s",
                                                     gmaps_device_get_link_text(type));

    when_true(amxd_object_get_instance_count(edge_template) > 0, exit, retval = true);

    if(include_alternatives
       && (gmaps_device_for_all_alternatives(device,
                                             gmaps_device_cb_check_link,
                                             (void*) type) == gmap_status_break)) {
        retval = true;
    }

exit:
    return retval;
}