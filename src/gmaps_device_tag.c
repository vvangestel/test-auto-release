/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <stdlib.h>
#include <string.h>

#include "gmaps_priv.h"

typedef struct cb_data {
    const char* tag;
    bool is_found;

} cb_data_t;

static gmap_traverse_status_t gmaps_device_has_tag_traverse_cb(amxd_object_t* device,
                                                               gmap_traverse_action_t action,
                                                               void* userdata) {
    cb_data_t* cb_data = (cb_data_t*) userdata;
    if(action == gmap_traverse_device_matching) {
        cb_data->is_found |= gmaps_device_traverse_has_tag(device, cb_data->tag);
    }

    return (cb_data->is_found ? gmap_traverse_done : gmap_traverse_continue);
}

gmap_status_t gmaps_device_has_tag(const char* key, const char* tag, const char* expression, gmap_traverse_mode_t mode, bool* result) {
    // ACL TODO: add ACL control when available in Ambiorix.
    gmap_status_t retval = gmap_status_unknown_error;
    gmap_traverse_status_t travresult;
    amxd_object_t* device = NULL;
    gmap_traverse_tree_cb_t cb = gmaps_device_has_tag_traverse_cb;
    cb_data_t cb_data = { .tag = tag, .is_found = false};

    when_null(result, exit, retval = gmap_status_invalid_parameter);
    when_str_empty(key, exit, retval = gmap_status_invalid_key);
    when_str_empty(tag, exit, retval = gmap_status_invalid_parameter);

    *result = false;

    device = gmaps_get_device(key);

    when_null(device, exit, retval = gmap_status_device_not_found);


    travresult = gmaps_traverse_tree(device, mode, expression, cb, (void*) &cb_data);

    *result = cb_data.is_found;
    retval = (travresult == gmap_traverse_done) ? gmap_status_ok : gmap_status_unknown_error;

exit:
    return retval;
}

static gmap_traverse_status_t gmaps_device_set_tag_traverse_cb(amxd_object_t* device,
                                                               gmap_traverse_action_t action,
                                                               void* userdata) {
    if(action == gmap_traverse_device_matching) {
        gmaps_device_traverse_set_tag(device, (ssv_string_t) userdata);
    }
    return gmap_traverse_continue;
}

gmap_status_t gmaps_device_set_tag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode) {
    // ACL TODO: add ACL control when available in Ambiorix.
    gmap_status_t retval = gmap_status_unknown_error;
    amxd_object_t* device = NULL;
    gmap_traverse_tree_cb_t cb = gmaps_device_set_tag_traverse_cb;
    gmap_traverse_status_t result = gmap_traverse_failed;
    when_str_empty(key, exit, retval = gmap_status_invalid_key);

    device = gmaps_get_device(key);

    when_null(device, exit, retval = gmap_status_device_not_found);

    result = gmaps_traverse_tree(device, mode, expression, cb, (void*) tags);

    retval = (result == gmap_traverse_done ? gmap_status_ok : gmap_status_unknown_error);

exit:
    return retval;
}

static gmap_traverse_status_t gmaps_device_clear_tag_traverse_cb(amxd_object_t* device,
                                                                 gmap_traverse_action_t action,
                                                                 void* userdata) {
    if(action == gmap_traverse_device_matching) {
        gmaps_device_traverse_clear_tag(device, (ssv_string_t) userdata);
    }
    return gmap_traverse_continue;
}

gmap_status_t GMAPS_PRIVATE gmaps_device_clear_tag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode) {
    gmap_status_t retval = gmap_status_unknown_error;
    amxd_object_t* device = NULL;
    gmap_traverse_tree_cb_t cb = gmaps_device_clear_tag_traverse_cb;
    gmap_traverse_status_t result = gmap_traverse_failed;
    when_str_empty(key, exit, retval = gmap_status_invalid_key);

    device = gmaps_get_device(key);

    when_null(device, exit, retval = gmap_status_device_not_found);

    result = gmaps_traverse_tree(device, mode, expression, cb, (void*) tags);

    retval = (result == gmap_traverse_done ? gmap_status_ok : gmap_status_unknown_error);

exit:
    return retval;
}