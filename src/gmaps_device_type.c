/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "gmaps_priv.h"


/**
   @ingroup gmap_device_types
   @brief
   Add a type to a device

   Server side imlementation to add a type.

   This function creates a instance of the "DeviceTypes" template object within the device object.

   Optionally the name of the source that adds the name can be given, if no source is provided (NULL pointer or empty string),
   the source "webui" is used. If a type was already available for the source, the type is overwritten with the new one.

   After that the type is added, the type selection algorithm is used to select the type of the device.
   (See @ref section_device_types_selection)

   @warning
   - This function can only be called on server side. Use @ref gmap_isServer to determine if the code is running on the
   server side.

   @param key the device key for which a new type is added
   @param type the new type
   @param source optionally the source that added the new type can be given

   @return
   true when the type has been added, false if it was not possible to add the type
   Check pcb_error for error conditions. Possible errors:
    - gmap_error_invalid_type: empty type or null pointer
    - pcb_error_not_found: device with given key not found.
    - gmap_error_commit_failed: failed to commit new type
 */
gmap_status_t gmaps_device_set_type(const char* key,
                                    const char* type,
                                    const char* source) {
    gmap_status_t retval = gmap_status_unknown_error;
    amxd_object_t* device = NULL;
    amxd_object_t* types_template = NULL;
    amxd_object_t* type_instance = NULL;
    amxd_trans_t* trans = NULL;
    char* old_type = NULL;

    amxd_trans_new(&trans);
    amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);

    when_str_empty(key, exit, retval = gmap_status_invalid_parameter);
    when_str_empty(type, exit, retval = gmap_status_invalid_parameter);
    when_str_empty(source, exit, retval = gmap_status_invalid_parameter);

    device = gmaps_get_device(key);
    when_null(device, exit, retval = gmap_status_device_not_found);
    types_template = amxd_object_get(device, "DeviceTypes");

    type_instance = amxd_object_get(types_template, source);
    if(type_instance != NULL) {
        // A type was already provided for the given source, so overwrite.
        old_type = amxd_object_get_value(cstring_t, type_instance, "Type", NULL);
        amxd_trans_select_object(trans, type_instance);
    } else {
        amxd_trans_select_object(trans, types_template);
        amxd_trans_add_inst(trans, 0, source);
    }

    if((old_type == NULL) || (strcmp(type, old_type) != 0)) {
        amxd_trans_set_value(cstring_t, trans, "Type", type);
        amxd_trans_set_value(cstring_t, trans, "Source", source);
        amxd_trans_apply(trans, gmap_get_dm());
    }

    retval = gmap_status_ok;
exit:
    amxd_trans_delete(&trans);
    free(old_type);
    return retval;
}

gmap_status_t gmaps_device_remove_type(const char* key, const char* source) {
    gmap_status_t retval = gmap_status_unknown_error;
    amxd_object_t* device = NULL;
    amxd_object_t* types_template = NULL;
    amxd_object_t* type_instance = NULL;

    when_str_empty(key, exit, retval = gmap_status_invalid_parameter);
    when_str_empty(source, exit, retval = gmap_status_invalid_parameter);

    device = gmaps_get_device(key);
    when_null(device, exit, retval = gmap_status_device_not_found);
    types_template = amxd_object_get(device, "DeviceTypes");

    type_instance = amxd_object_get(types_template, source);
    if(type_instance != NULL) {
        amxd_trans_t trans;
        amxd_status_t result = amxd_status_unknown_error;
        amxd_trans_init(&trans);

        amxd_trans_select_object(&trans, types_template);
        amxd_trans_del_inst(&trans, 0, source);

        result = amxd_trans_apply(&trans, gmap_get_dm());

        retval = (result == amxd_status_ok ? gmap_status_ok : gmap_status_unknown_error);

        amxd_trans_clean(&trans);
    }

exit:

    return retval;
}