/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <stdlib.h>
#include <string.h>

#include "gmaps_priv.h"


static void gmaps_device_set_consume_active(const char* key,
                                            amxc_var_t* values) {
    amxc_var_t* data = amxc_var_get_key(values, "Active", AMXC_VAR_FLAG_DEFAULT);

    while(data != NULL) {
        gmaps_device_set_active(key, amxc_var_dyncast(bool, data));
        amxc_var_delete(&data);
        data = amxc_var_get_key(values, "Active", AMXC_VAR_FLAG_DEFAULT);
    }
}

static void gmaps_device_set_consume_name(amxd_object_t* device,
                                          amxc_var_t* values) {
    amxc_var_t* data = amxc_var_get_key(values, "Name", AMXC_VAR_FLAG_DEFAULT);

    while(data != NULL) {
        const char* name = amxc_var_constcast(cstring_t, data);
        char* old_name = amxd_object_get_value(cstring_t, device, "Name", NULL);
        if((name == NULL) || (old_name == NULL) || (strcmp(name, old_name) != 0)) {
            // set the name only if it's a new one (to not erase the Source with "webui")
            gmaps_device_set_name(device, name, "webui");
        }
        free(old_name);
        amxc_var_delete(&data);
        data = amxc_var_get_key(values, "Name", AMXC_VAR_FLAG_DEFAULT);
    }
}

static void gmaps_device_set_consume_type(const char* key,
                                          amxd_object_t* device,
                                          amxc_var_t* values) {
    amxc_var_t* data = amxc_var_get_key(values, "DeviceType", AMXC_VAR_FLAG_DEFAULT);

    while(data != NULL) {
        const char* type = amxc_var_constcast(cstring_t, data);
        char* old_type = amxd_object_get_value(cstring_t, device, "Type", NULL);
        if((type == NULL) || (old_type == NULL) || (strcmp(type, old_type) != 0)) {
            // set the type only if it's a new one (to not erase the Source with "webui")
            gmaps_device_set_type(key, type, "webui");
        }
        free(old_type);
        amxc_var_delete(&data);
        data = amxc_var_get_key(values, "DeviceType", AMXC_VAR_FLAG_DEFAULT);
    }
}

static void gmaps_device_set_consume_key(amxc_var_t* values) {
    amxc_var_t* data = amxc_var_get_key(values, "Key", AMXC_VAR_FLAG_DEFAULT);

    while(data != NULL) {
        amxc_var_delete(&data);
        data = amxc_var_get_key(values, "Key", AMXC_VAR_FLAG_DEFAULT);
    }
}

/**
 * This function handles and "filters" out the following keys from the provided @param values.
 *      - "Active"
 *      - "Key"
 *      - "Name"
 *      - "DeviceType"
 */
static void gmaps_device_set_internal_filter(amxd_object_t* device,
                                             amxc_var_t* values) {
    // TODO_ACL TODO: add ACL control when available in Ambiorix.
    const char* key = amxd_object_get_name(device, AMXD_OBJECT_NAMED);

    gmaps_device_set_consume_active(key, values);
    gmaps_device_set_consume_name(device, values);
    gmaps_device_set_consume_type(key, device, values);
    gmaps_device_set_consume_key(values);
}

static void gmaps_device_set_recursive(amxd_object_t* object,
                                       amxd_trans_t* transaction,
                                       const amxc_var_t* values) {
    amxc_htable_for_each(hit, amxc_var_constcast(amxc_htable_t, values)) {
        const char* name = amxc_htable_it_get_key(hit);
        amxc_var_t* value = amxc_htable_it_get_data(hit, amxc_var_t, hit);

        amxd_trans_select_object(transaction, object);

        if(amxc_var_type_of(value) == AMXC_VAR_ID_HTABLE) {
            amxd_object_t* child = amxd_object_get(object, name);
            if(child == NULL) {
                if(amxd_object_get_type(object) == amxd_object_template) {
                    amxd_trans_add_inst(transaction, 0, name);
                } else {
                    // Not found, skip it silently.
                    continue;
                }
            }
            gmaps_device_set_recursive(child, transaction, value);
        } else {
            amxd_param_t* param = amxd_object_get_param_def(object, name);
            if(param == NULL) {
                continue;
            }
            amxd_trans_set_param(transaction, name, value);
        }
    }
}

static void gmaps_device_set_internal(amxd_object_t* device,
                                      amxc_var_t* values) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    gmaps_device_set_recursive(device, &trans, values);
    amxd_trans_apply(&trans, gmap_get_dm());

    amxd_trans_clean(&trans);
}

gmap_status_t gmaps_device_set(const char* key,
                               amxc_var_t* values) {
    // TODO_ACL TODO: add ACL control when available in Ambiorix.
    gmap_status_t retval = gmap_status_unknown_error;
    amxd_object_t* device = NULL;

    when_str_empty(key, exit, retval = gmap_status_invalid_key);
    when_true(amxc_var_is_null(values), exit, retval = gmap_status_invalid_parameter);

    device = gmaps_get_device(key);
    when_null(device, exit, retval = gmap_status_device_not_found);

    gmaps_device_set_internal_filter(device, values);
    gmaps_device_set_internal(device, values);

    retval = gmap_status_ok;
exit:
    return retval;
}