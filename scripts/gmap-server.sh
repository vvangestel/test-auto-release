#!/bin/sh

name="gmap-server"

case $1 in
    start)
            $name &
        ;;
    stop)
        killall $name
        ;;
    boot)
	    $name &
	;;
    shutdown)
	killall $name
	;;
esac
