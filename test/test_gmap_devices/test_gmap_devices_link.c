/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_devices.h"
#include "gmaps_dm_devices.h"


/**
 * Links devices with the given parameters and performs some sanity checks on the result.
 * The provided parameters should be valid.
 * @param new_link  If true, the function will check the new link. Else, it will check
 *                  that no new link has been created.
 */
void util_link_devices(const char* upper_key, const char* lower_key, bool new_link) {
    amxd_object_t* devices = amxd_dm_findf(test_gmap_get_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;

    amxd_object_t* upper = amxd_object_get_instance(amxd_object_get_child(devices, "Device"),
                                                    upper_key,
                                                    0);
    amxd_object_t* upper_ldevice_template = amxd_object_findf(upper, "LDevice");
    amxd_object_t* upper_udevice_template = amxd_object_findf(upper, "UDevice");
    uint32_t udevice_llink_count = amxd_object_get_instance_count(upper_ldevice_template);
    uint32_t udevice_ulink_count = amxd_object_get_instance_count(upper_udevice_template);

    amxd_object_t* lower = amxd_object_get_instance(amxd_object_get_child(devices, "Device"),
                                                    lower_key,
                                                    0);
    amxd_object_t* lower_ldevice_template = amxd_object_findf(lower, "LDevice");
    amxd_object_t* lower_udevice_template = amxd_object_findf(lower, "UDevice");
    uint32_t ldevice_llink_count = amxd_object_get_instance_count(lower_ldevice_template);
    uint32_t ldevice_ulink_count = amxd_object_get_instance_count(lower_udevice_template);

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "upper_device", upper_key);
    amxc_var_add_key(cstring_t, &args, "lower_device", lower_key);

    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "setLink",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // check return value
    assert_true(amxc_var_get_bool(&ret));

    // readback, verify, dump of upper device
    assert_int_equal(amxd_object_get_instance_count(upper_ldevice_template),
                     udevice_llink_count + (uint32_t) new_link);
    assert_int_equal(amxd_object_get_instance_count(upper_udevice_template),
                     udevice_ulink_count);

    amxd_object_t* upper_ldevice = amxd_object_get_instance(upper_ldevice_template,
                                                            lower_key, 0);
    const char* upper_ldevice_name = amxd_object_get_name(upper_ldevice, AMXD_OBJECT_NAMED);
    assert_string_equal(upper_ldevice_name, lower_key);
    amxd_object_get_params(upper_ldevice, &params, amxd_dm_access_protected);
    amxc_var_dump(&params, STDOUT_FILENO);

    // readback, verify, dump of lower device
    assert_int_equal(amxd_object_get_instance_count(lower_ldevice_template),
                     ldevice_llink_count);
    assert_int_equal(amxd_object_get_instance_count(lower_udevice_template),
                     ldevice_ulink_count + (uint32_t) new_link);

    amxd_object_t* lower_udevice = amxd_object_get_instance(lower_udevice_template,
                                                            upper_key, 0);
    assert_non_null(lower_udevice);
    const char* lower_udevice_name = amxd_object_get_name(lower_udevice, AMXD_OBJECT_NAMED);
    assert_string_equal(lower_udevice_name, upper_key);
    amxd_object_get_params(lower_udevice, &params, amxd_dm_access_protected);
    amxc_var_dump(&params, STDOUT_FILENO);
    printf("UDevice [%s] has LDevice [%s]\n", lower_udevice_name, upper_ldevice_name);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void test_rpc_can_link_devices(GMAPS_UNUSED void** state) {
    util_link_devices("testKey", "test2Key", true);
    amxd_object_t* dev1 = gmaps_get_device("testKey");
    amxd_object_t* dev2 = gmaps_get_device("test2Key");
    assert_true(gmaps_device_is_linked_to(dev1, dev2, gmap_traverse_down));
    assert_true(gmaps_device_is_linked_to(dev2, dev1, gmap_traverse_up));

    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "device", "test2Key");
    amxc_var_add_key(cstring_t, &args, "traverse", "down");

    assert_int_equal(amxd_object_invoke_function(dev1,
                                                 "isLinkedTo",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // check return value
    assert_true(amxc_var_get_bool(&ret));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);


}

void test_rpc_can_link_devices_again_same(GMAPS_UNUSED void** state) {
    util_link_devices("testKey", "test2Key", false);

    amxd_object_t* dev1 = gmaps_get_device("testKey");
    amxd_object_t* dev2 = gmaps_get_device("test2Key");

    assert_true(gmaps_device_is_linked_to(dev1, dev2, gmap_traverse_down));
    assert_true(gmaps_device_is_linked_to(dev2, dev1, gmap_traverse_up));

    gmaps_device_removeLLink(dev1, "test2Key");
    gmaps_device_removeULink(dev2, "testKey");

    assert_false(gmaps_device_is_linked_to(dev1, dev2, gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(dev2, dev1, gmap_traverse_up));

    util_link_devices("testKey", "test2Key", true);

    gmaps_device_removeAllLLink(dev1, NULL);
    gmaps_device_removeAllULink(dev2, NULL);

    assert_false(gmaps_device_is_linked_to(dev1, dev2, gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(dev2, dev1, gmap_traverse_up));
}

void test_rpc_link_devices_input_validation(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(test_gmap_get_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "upper_device", "madeUpKey");
    amxc_var_add_key(cstring_t, &args, "lower_device", "test2Key");

    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "setLink",
                                                 &args,
                                                 &ret),
                     amxd_status_unknown_error);

    // check return value
    assert_false(amxc_var_get_bool(&ret));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_internal_link_devices_input_validation(GMAPS_UNUSED void** state) {
    assert_int_equal(gmaps_device_make_link(NULL,
                                            "test2Key",
                                            "testType",
                                            false),
                     gmap_status_device_not_found);
    assert_int_equal(gmaps_device_make_link("testKey",
                                            NULL,
                                            "testType",
                                            false),
                     gmap_status_device_not_found);
    assert_int_equal(gmaps_device_make_link("testKey",
                                            "test2Key",
                                            NULL,
                                            false),
                     gmap_status_invalid_parameter);
}