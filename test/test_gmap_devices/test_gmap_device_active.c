/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_dm_device.h"
#include "gmaps_device_type.h"

void util_set_active(const char* key,
                     bool active,
                     bool use_default,
                     bool expect_last_connection_update) {
    amxd_object_t* device = amxd_dm_findf(test_gmap_get_dm(), "Devices.Device.%s", key);
    amxc_var_t args;
    amxc_var_t ret;
    amxc_ts_t t_before;
    amxc_ts_t t_after;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_ts_now(&t_before);

    assert_ptr_not_equal(device, NULL);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    if(use_default == false) {
        amxc_var_add_key(bool, &args, "active", active);
    } else {
        assert_true(active);
    }

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "setActive",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_ts_now(&t_after);

    // readback, verify
    bool active_read = amxd_object_get_value(bool, device, "Active", NULL);
    assert_int_equal(active_read, active);
    if(expect_last_connection_update) {
        // The LastConnection timestamp should be updated,
        // so between t_before and t_after.
        amxc_ts_t* t_read = amxd_object_get_value(amxc_ts_t, device, "LastConnection", NULL);
        assert_int_not_equal(amxc_ts_compare(&t_before, t_read), 1);
        assert_int_not_equal(amxc_ts_compare(&t_after, t_read), -1);
        free(t_read);
    }

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_rpc_can_set_active(GMAPS_UNUSED void** state) {
    util_set_active("testKey", true, false, false);
    util_set_active("testKey", false, false, false);
    util_set_active("testKey", true, false, true);
    util_set_active("testKey", true, true, false);
}

void test_internal_set_active_input_validation(GMAPS_UNUSED void** state) {
    assert_int_equal(gmaps_device_set_active(NULL, true),
                     gmap_status_invalid_key);
    assert_int_equal(gmaps_device_set_active("", true),
                     gmap_status_invalid_key);
    assert_int_equal(gmaps_device_set_active("madeUpKey", true),
                     gmap_status_device_not_found);
}