/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __TEST_DEVICES_H__
#define __TEST_DEVICES_H__


#include "../test_common/test_common.h"


void util_create_device(const char* const key,
                        const char* const discovery_source,
                        const char* const tags,
                        bool persistent,
                        const char* const default_name,
                        amxd_status_t expected_status, amxc_var_t* values);
void test_rpc_can_create_device_first(void** state);
void test_rpc_can_create_device_second(void** state);
void test_rpc_can_create_device_third(void** state);
void test_rpc_can_create_device_extra(void** state);
void test_rpc_can_create_device_temp(void** state);
void test_internal_create_device_input_validation(void** state);

void test_rpc_can_destroy_device(void** state);
void test_internal_destroy_device_input_validation(void** state);

void test_rpc_can_find_device_by_parameter(void** state);
void test_rpc_cannot_find_device_by_parameter_false(void** state);
void test_rpc_cannot_find_device_by_nonexisting_parameter_true(void** state);
void test_rpc_cannot_find_device_by_nonexisting_parameter_false(void** state);
void test_rpc_can_find_device_by_parameter_htable(void** state);
void test_rpc_can_find_device_by_tag(void** state);
void test_rpc_can_find_devices_by_tag(void** state);
void test_rpc_can_find_device_by_tags(void** state);
void test_internal_find_device_input_validation(void** state);

void util_link_devices(const char* upper_key, const char* lower_key, bool new_link);
void test_rpc_can_link_devices(void** state);
void test_rpc_can_link_devices_again_same(void** state);
void test_rpc_link_devices_input_validation(void** state);
void test_internal_link_devices_input_validation(void** state);

/**
 *  Test functions for Devices get
 */
void test_gmap_can_devices_get(void** state);
void test_gmap_can_devices_get_result(void** state);
void test_gmap_can_invoke_devices_get(void**);


void test_rpc_can_create_tree0(void** state);
void test_rpc_can_create_tree1(void** state);
void test_internal_traverse_tree0_dev0_all_match(void** state);
void test_internal_traverse_tree0_dev0_all_match_early_stop(void** state);
void test_internal_traverse_tree0_dev0_this_early_stop(void** state);
void test_internal_traverse_tree0_dev0_early_stop_at_push_cb(void** state);
void test_internal_traverse_tree1_recursive(void** state);
void test_internal_traverse_input_validation(void** state);
void test_internal_traverse_mode_string_conversions(void** state);

/**
 * Testfunctions for test_gmap_device_type.c
 */
void util_set_type(const char* key, const char* type, const char* source);
void test_rpc_can_set_type_new(void** state);
void test_internal_set_type_input_validation(void** state);
void test_rpc_can_set_type_overwrite(void** state);
void test_rpc_can_set_type_default_source(void** state);
void test_we_can_remove_type(void** state);
void test_rpc_can_remove_type(void** state);




void test_internal_can_add_names_to_table(void** state);
void test_internal_can_add_names_to_table_existing(void** state);
void test_internal_can_get_first_available_index(void** state);
void test_internal_can_get_index_available(void** state);
void test_internal_can_remove_names_from_table(void** state);

void util_set_name(const char* const key,
                   const char* const name,
                   const char* const source,
                   const char* const expected_name,
                   const char* const expected_source,
                   const char* const expected_suffix_char,
                   uint32_t expected_suffix_uint);
void test_rpc_can_set_name(void** state);
void test_internal_set_name_input_validation(void** state);

void util_set_active(const char* key,
                     bool active,
                     bool use_default,
                     bool expect_last_connection_update);
void test_rpc_can_set_active(void** state);
void test_internal_set_active_input_validation(void** state);

void util_block_device(const char* key);
void util_unblock_device(const char* key);
bool util_is_blocked_device(const char* key);
void test_rpc_can_block_unblock_device(void** state);
void test_internal_block_device_input_validation(void** state);
void test_internal_unblock_device_input_validation(void** state);
void test_internal_is_blocked_device_input_validation(void** state);
void test_rpc_can_block_device_already_blocked(void** state);
void test_rpc_LLTD_device_is_blocked(void** state);
void test_rpc_can_unblock_device_already_unblocked(void** state);

void test_rpc_can_set_parameter_active_false(void** state);
void test_rpc_can_set_parameter_active_true(void** state);
void test_rpc_can_set_parameter_name(void** state);
void test_rpc_can_set_parameter_name_null(void** state);
void test_rpc_can_set_parameter_type(void** state);
void test_rpc_can_set_parameter_type_null(void** state);
void test_rpc_cannot_set_parameter_key(void** state);
void test_rpc_can_set_parameters_unfiltered(void** state);
void test_rpc_can_set_parameters_unfiltered_recursive(void** state);
void test_internal_set_input_validation(void** state);

/**
 * Test function for device tag
 */
void test_gmap_can_device_set_tag(void** state);
void test_gmap_duplicate_tags_are_ignored(void** state);
void test_gmap_can_invoke_device_set_tag(void** state);
void test_gmap_can_clear_tag(void** state);
void test_gmap_can_invoke_clear_tag(void** state);

void test_gmap_can_device_has_tag(void** state);
void test_gmap_can_invoke_device_has_tag(void** state);

/**
 *  Test functions for Device get
 */
void test_gmap_can_device_get(void** state);
void test_gmap_can_device_get_result(void** state);
void test_gmap_can_invoke_device_get(void**);

void test_gmap_can_device_set_tag(void** state);
void test_gmap_can_device_has_tag(void** state);
void test_gmap_can_invoke_device_has_tag(void** state);

void test_gmap_can_alternative_wrong_parameters(void** state);
void test_gmap_can_device_set_alternative(void** state);
void test_gmap_can_invoke_set_alternative(void** state);
void test_gmap_can_device_remove_alternative(void** state);
void test_gmap_can_invoke_remove_alternatives(void** state);



#endif  // __TEST_DEVICES_H__
