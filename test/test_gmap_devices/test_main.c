/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"


int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_rpc_can_create_device_first),
        cmocka_unit_test(test_rpc_can_create_device_second),
        cmocka_unit_test(test_rpc_can_create_device_third),
        cmocka_unit_test(test_rpc_can_create_device_temp),
        cmocka_unit_test(test_rpc_can_create_device_extra),
        cmocka_unit_test(test_internal_create_device_input_validation),

        cmocka_unit_test(test_rpc_can_destroy_device),
        cmocka_unit_test(test_internal_destroy_device_input_validation),

        cmocka_unit_test(test_rpc_can_find_device_by_parameter),
        cmocka_unit_test(test_rpc_cannot_find_device_by_parameter_false),
        cmocka_unit_test(test_rpc_cannot_find_device_by_nonexisting_parameter_true),
        cmocka_unit_test(test_rpc_cannot_find_device_by_nonexisting_parameter_false),
        cmocka_unit_test(test_rpc_can_find_device_by_parameter_htable),
        cmocka_unit_test(test_rpc_can_find_device_by_tag),
        cmocka_unit_test(test_rpc_can_find_devices_by_tag),
        cmocka_unit_test(test_rpc_can_find_device_by_tags),
        cmocka_unit_test(test_internal_find_device_input_validation),

        cmocka_unit_test(test_rpc_can_link_devices),
        cmocka_unit_test(test_rpc_can_link_devices_again_same),
        cmocka_unit_test(test_rpc_link_devices_input_validation),
        cmocka_unit_test(test_internal_link_devices_input_validation),

        cmocka_unit_test(test_rpc_can_create_tree0),
        cmocka_unit_test(test_rpc_can_create_tree1),
        cmocka_unit_test(test_internal_traverse_tree0_dev0_all_match),
        cmocka_unit_test(test_internal_traverse_tree0_dev0_all_match_early_stop),
        cmocka_unit_test(test_internal_traverse_tree0_dev0_this_early_stop),
        cmocka_unit_test(test_internal_traverse_tree0_dev0_early_stop_at_push_cb),
        cmocka_unit_test(test_internal_traverse_tree1_recursive),
        cmocka_unit_test(test_internal_traverse_input_validation),
        cmocka_unit_test(test_internal_traverse_mode_string_conversions),


        /**
         * Run tests from test_gmap_device_type.c
         */
        cmocka_unit_test(test_rpc_can_set_type_new),
        cmocka_unit_test(test_rpc_can_set_type_overwrite),
        cmocka_unit_test(test_rpc_can_set_type_default_source),
        cmocka_unit_test(test_internal_set_type_input_validation),
        cmocka_unit_test(test_we_can_remove_type),
        cmocka_unit_test(test_rpc_can_remove_type),


        cmocka_unit_test(test_internal_can_add_names_to_table),
        cmocka_unit_test(test_internal_can_add_names_to_table_existing),
        cmocka_unit_test(test_internal_can_get_first_available_index),
        cmocka_unit_test(test_internal_can_get_index_available),
        cmocka_unit_test(test_internal_can_remove_names_from_table),

        cmocka_unit_test(test_rpc_can_set_name),
        cmocka_unit_test(test_internal_set_name_input_validation),

        cmocka_unit_test(test_rpc_can_set_active),
        cmocka_unit_test(test_internal_set_active_input_validation),

        cmocka_unit_test(test_rpc_can_block_unblock_device),
        cmocka_unit_test(test_rpc_can_block_device_already_blocked),
        cmocka_unit_test(test_rpc_can_unblock_device_already_unblocked),
        cmocka_unit_test(test_rpc_LLTD_device_is_blocked),
        cmocka_unit_test(test_internal_block_device_input_validation),
        cmocka_unit_test(test_internal_unblock_device_input_validation),
        cmocka_unit_test(test_internal_is_blocked_device_input_validation),

        cmocka_unit_test(test_rpc_can_set_parameter_active_false),
        cmocka_unit_test(test_rpc_can_set_parameter_active_true),
        cmocka_unit_test(test_rpc_can_set_parameter_name),
        cmocka_unit_test(test_rpc_can_set_parameter_name_null),
        cmocka_unit_test(test_rpc_can_set_parameter_type),
        cmocka_unit_test(test_rpc_can_set_parameter_type_null),
        cmocka_unit_test(test_rpc_cannot_set_parameter_key),
        cmocka_unit_test(test_rpc_can_set_parameters_unfiltered),
        cmocka_unit_test(test_rpc_can_set_parameters_unfiltered_recursive),
        cmocka_unit_test(test_internal_set_input_validation),

        /**
         *  Run tests for tags
         */
        cmocka_unit_test(test_gmap_can_device_set_tag),
        cmocka_unit_test(test_gmap_duplicate_tags_are_ignored),
        cmocka_unit_test(test_gmap_can_invoke_device_set_tag),
        cmocka_unit_test(test_gmap_can_device_has_tag),
        cmocka_unit_test(test_gmap_can_invoke_device_has_tag),
        cmocka_unit_test(test_gmap_can_clear_tag),
        cmocka_unit_test(test_gmap_can_invoke_clear_tag),

        /**
         * Ttests for devices get
         */
        cmocka_unit_test(test_gmap_can_devices_get),
        cmocka_unit_test(test_gmap_can_devices_get_result),
        cmocka_unit_test(test_gmap_can_invoke_devices_get),

        /**
         * Tests for device get
         */
        cmocka_unit_test(test_gmap_can_device_get),
        cmocka_unit_test(test_gmap_can_device_get_result),
        cmocka_unit_test(test_gmap_can_invoke_device_get),

        cmocka_unit_test(test_gmap_can_alternative_wrong_parameters),
        cmocka_unit_test(test_gmap_can_device_set_alternative),
        cmocka_unit_test(test_gmap_can_device_remove_alternative),
        cmocka_unit_test(test_gmap_can_invoke_set_alternative),
        cmocka_unit_test(test_gmap_can_invoke_remove_alternatives),

    };
    return cmocka_run_group_tests(tests, test_gmap_setup, test_gmap_teardown);
}
