/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_dm_devices.h"


static const char* odl_defs = "../../odl/gmap-server-definitions.odl";
static const char* odl_test_config = "../gmap-test-defaults.odl";
static amxo_parser_t parser;
static amxd_dm_t dm;
static bool initialized;


amxd_dm_t* test_gmap_get_dm(void) {
    assert_true(initialized);
    return &dm;
}

int test_gmap_setup(GMAPS_UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_ptr_not_equal(root_obj, NULL);

    amxo_resolver_ftab_add(&parser, "createDevice", AMXO_FUNC(_Devices_createDevice));
    amxo_resolver_ftab_add(&parser, "destroyDevice", AMXO_FUNC(_Devices_destroyDevice));
    amxo_resolver_ftab_add(&parser, "find", AMXO_FUNC(_Devices_find));
    amxo_resolver_ftab_add(&parser, "setLink", AMXO_FUNC(_Devices_setLink));
    amxo_resolver_ftab_add(&parser, "block", AMXO_FUNC(_Devices_block));
    amxo_resolver_ftab_add(&parser, "unblock", AMXO_FUNC(_Devices_unblock));
    amxo_resolver_ftab_add(&parser, "isBlocked", AMXO_FUNC(_Devices_isBlocked));
    amxo_resolver_ftab_add(&parser, "get", AMXO_FUNC(_Devices_get));

    amxo_resolver_ftab_add(&parser, "setType", AMXO_FUNC(_Device_setType));
    amxo_resolver_ftab_add(&parser, "removeType", AMXO_FUNC(_Device_removeType));

    amxo_resolver_ftab_add(&parser, "setName", AMXO_FUNC(_Device_setName));
    amxo_resolver_ftab_add(&parser, "setActive", AMXO_FUNC(_Device_setActive));
    amxo_resolver_ftab_add(&parser, "set", AMXO_FUNC(_Device_set));
    amxo_resolver_ftab_add(&parser, "get", AMXO_FUNC(_Device_get));
    amxo_resolver_ftab_add(&parser, "setTag", AMXO_FUNC(_Device_setTag));
    amxo_resolver_ftab_add(&parser, "hasTag", AMXO_FUNC(_Device_hasTag));
    amxo_resolver_ftab_add(&parser, "clearTag", AMXO_FUNC(_Device_clearTag));
    amxo_resolver_ftab_add(&parser, "setAlternative", AMXO_FUNC(_Device_setAlternative));
    amxo_resolver_ftab_add(&parser, "removeAlternative", AMXO_FUNC(_Device_removeAlternative));
    amxo_resolver_ftab_add(&parser, "isAlternative", AMXO_FUNC(_Device_isAlternative));
    amxo_resolver_ftab_add(&parser, "isLinkedTo", AMXO_FUNC(_Device_isLinkedTo));


    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_test_config, root_obj), 0);

    gmap_server_init(&dm, &parser);

    initialized = true;

    return 0;
}

int test_gmap_teardown(GMAPS_UNUSED void** state) {
    gmap_server_cleanup();

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    initialized = false;

    return 0;
}
