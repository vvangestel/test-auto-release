/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_dm_device.h"
#include "gmaps_device_type.h"


void util_set_type(const char* key, const char* type, const char* source) {
    amxd_object_t* device = amxd_dm_findf(test_gmap_get_dm(), "Devices.Device.%s", key);
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_ptr_not_equal(device, NULL);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", type);
    if(source != NULL) {
        amxc_var_add_key(cstring_t, &args, "source", source);
    }

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "setType",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // check return value
    assert_true(amxc_var_get_bool(&ret));

    // readback, verify
    if((source == NULL) || (*source == 0)) {
        source = "webui";
    }
    amxd_object_t* type_instance = amxd_object_findf(device, "DeviceTypes.%s", source);
    assert_non_null(type_instance);
    char* type_read = amxd_object_get_value(cstring_t, type_instance, "Type", NULL);
    assert_string_equal(type_read, type);
    char* source_read = amxd_object_get_value(cstring_t, type_instance, "Source", NULL);
    assert_string_equal(source_read, source);

    free(type_read);
    free(source_read);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_rpc_can_set_type_new(GMAPS_UNUSED void** state) {
    util_set_type("testKey", "testType", "testSource");
}

void test_rpc_can_set_type_overwrite(GMAPS_UNUSED void** state) {
    util_set_type("testKey", "testTypeOverwrite", "testSource");
}

void test_rpc_can_set_type_default_source(GMAPS_UNUSED void** state) {
    util_set_type("testKey", "testTypeOverwrite", NULL);
    util_set_type("testKey", "testTypeOverwrite", "");
}

void test_internal_set_type_input_validation(GMAPS_UNUSED void** state) {
    assert_int_equal(gmaps_device_set_type(NULL, "myType", "mySource"),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_device_set_type("", "myType", "mySource"),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_device_set_type("myKey", NULL, "mySource"),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_device_set_type("myKey", "", "mySource"),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_device_set_type("myKey", "myType", NULL),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_device_set_type("myKey", "myType", ""),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_device_set_type("madeUpKey", "myType", "mySource"),
                     gmap_status_device_not_found);
}

void test_we_can_remove_type(GMAPS_UNUSED void** state) {
    const char* source = "testSource";
    amxd_object_t* device = amxd_dm_findf(test_gmap_get_dm(), "Devices.Device.%s", "testKey");
    amxd_object_t* type_instance = amxd_object_findf(device, "DeviceTypes.%s", source);

    assert_non_null(type_instance);

    gmaps_device_remove_type("testKey", source);

    device = amxd_dm_findf(test_gmap_get_dm(), "Devices.Device.%s", "testKey");
    type_instance = amxd_object_findf(device, "DeviceTypes.%s", source);

    assert_null(type_instance);

    util_set_type("testKey", "myType", source);
}

void test_rpc_can_remove_type(GMAPS_UNUSED void** state) {

    amxd_object_t* device = gmaps_get_device("testKey");
    amxc_var_t args;
    amxc_var_t result;
    const char* source = "testSource";
    amxd_object_t* type_instance = amxd_object_findf(device, "DeviceTypes.%s", source);

    assert_non_null(type_instance);

    amxc_var_init(&args);
    amxc_var_init(&result);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "source", source);

    assert_non_null(type_instance);

    amxd_object_invoke_function(device, "removeType", &args, &result);

    device = amxd_dm_findf(test_gmap_get_dm(), "Devices.Device.%s", "testKey");
    type_instance = amxd_object_findf(device, "DeviceTypes.%s", source);

    assert_null(type_instance);


    amxc_var_clean(&args);
    amxc_var_clean(&result);

    util_set_type("testKey", "myType", source);

}