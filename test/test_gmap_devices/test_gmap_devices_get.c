/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_dm_devices.h"
#include "gmaps_devices.h"



void test_gmap_can_devices_get(GMAPS_UNUSED void** state) {

    amxc_var_t* test = gmaps_devices_get("", 0);

    assert_non_null(test);
    assert_int_equal(AMXC_VAR_ID_LIST, test->type_id);

    amxc_var_delete(&test);


}

void test_gmap_can_devices_get_result(GMAPS_UNUSED void** state) {


    amxc_var_t* test = gmaps_devices_get(".Key==\"test2Key\"", 0);
    int count = 0;

    amxc_var_dump(test, STDOUT_FILENO);

    assert_non_null(test);

    assert_int_equal(AMXC_VAR_ID_LIST, test->type_id);

    amxc_var_for_each(result, test) {
        assert_int_equal(result->type_id, AMXC_VAR_ID_HTABLE);
        char* key = amxc_var_dyncast(cstring_t, amxc_var_get_key(result, "Key", 0));
        assert_string_equal(key, "test2Key");
        count++;
        free(key);
    }

    assert_int_equal(count, 1);

    amxc_var_delete(&test);
}

/**
 *  Test if we correctly can invoke the get function
 */
void test_gmap_can_invoke_devices_get(GMAPS_UNUSED void** state) {
    amxd_object_t* device = amxd_dm_findf(gmap_get_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t result;

    amxc_var_init(&args);
    amxc_var_init(&result);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "expression", "test2Key");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_non_null(device);

    amxd_object_invoke_function(device, "get", &args, &result);

    assert_int_equal(AMXC_VAR_ID_LIST, result.type_id);

    amxc_var_clean(&args);
    amxc_var_clean(&result);


}