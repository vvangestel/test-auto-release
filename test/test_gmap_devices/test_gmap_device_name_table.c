/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_dm_device.h"


void test_internal_can_add_names_to_table(GMAPS_UNUSED void** state) {
    gmaps_device_name_table_add("testName0", 0);

    gmaps_device_name_table_add("testName1", 1);
    gmaps_device_name_table_add("testName1", 2);

    gmaps_device_name_table_add("testName2", 2);
    gmaps_device_name_table_add("testName2", 3);
    gmaps_device_name_table_add("testName2", 4);
}

void test_internal_can_add_names_to_table_existing(GMAPS_UNUSED void** state) {
    gmaps_device_name_table_add("testName0", 0);
}

void test_internal_can_get_first_available_index(GMAPS_UNUSED void** state) {
    assert_int_equal(gmaps_device_name_table_get_first_available_index("testName0"),
                     1);
    assert_int_equal(gmaps_device_name_table_get_first_available_index("testName1"),
                     0);
    assert_int_equal(gmaps_device_name_table_get_first_available_index("testName2"),
                     0);
}

void test_internal_can_get_index_available(GMAPS_UNUSED void** state) {
    assert_true(gmaps_device_name_table_is_index_available("testName0", 1));
    assert_true(gmaps_device_name_table_is_index_available("testName0", 2));
    assert_true(gmaps_device_name_table_is_index_available("testName0", 3));

    assert_true(gmaps_device_name_table_is_index_available("testName1", 0));
    assert_true(gmaps_device_name_table_is_index_available("testName1", 3));

    assert_true(gmaps_device_name_table_is_index_available("testName2", 0));
    assert_true(gmaps_device_name_table_is_index_available("testName2", 1));
    assert_true(gmaps_device_name_table_is_index_available("testName2", 5));

    assert_false(gmaps_device_name_table_is_index_available("testName0", 0));
    assert_false(gmaps_device_name_table_is_index_available("testName1", 1));
    assert_false(gmaps_device_name_table_is_index_available("testName1", 2));
    assert_false(gmaps_device_name_table_is_index_available("testName2", 2));
    assert_false(gmaps_device_name_table_is_index_available("testName2", 3));
    assert_false(gmaps_device_name_table_is_index_available("testName2", 4));
}

void test_internal_can_remove_names_from_table(GMAPS_UNUSED void** state) {
    assert_true(gmaps_device_name_table_remove("testName0", 0));
    assert_int_equal(gmaps_device_name_table_get_first_available_index("testName0"),
                     1);  // index 0 was a double
    assert_true(gmaps_device_name_table_remove("testName0", 0));
    assert_int_equal(gmaps_device_name_table_get_first_available_index("testName0"),
                     0);  // now it should be gone
    assert_true(gmaps_device_name_table_is_index_available("testName0", 0));

    assert_false(gmaps_device_name_table_remove("testName1", 0));
    assert_true(gmaps_device_name_table_remove("testName1", 1));
    assert_int_equal(gmaps_device_name_table_get_first_available_index("testName1"),
                     0);
    assert_true(gmaps_device_name_table_is_index_available("testName1", 1));
    assert_false(gmaps_device_name_table_is_index_available("testName1", 2));

    assert_false(gmaps_device_name_table_remove("madeUpName", 0));
}