/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_devices.h"
#include "gmaps_dm_devices.h"


static void set_args_create_device(amxc_var_t* args,
                                   const char* const key,
                                   const char* const discovery_source,
                                   const char* const tags,
                                   bool persistent,
                                   const char* const default_name,
                                   amxc_var_t* parameters) {
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, "key", key);
    amxc_var_add_key(cstring_t, args, "discovery_source", discovery_source);
    amxc_var_add_key(cstring_t, args, "tags", tags);
    amxc_var_add_key(bool, args, "persistent", persistent);
    amxc_var_add_key(cstring_t, args, "default_name", default_name);
    if(parameters != NULL) {
        amxc_var_t* values = amxc_var_add_new_key(args, "values");
        amxc_var_copy(values, parameters);
    }
}

/**
 * Creates a devices with the given parameters and performs some sanity checks on the result.
 * The provided parameters should be valid.
 */
void util_create_device(const char* const key,
                        const char* const discovery_source,
                        const char* const tags,
                        bool persistent,
                        const char* const default_name,
                        amxd_status_t expected_status,
                        amxc_var_t* values) {
    amxd_object_t* devices = amxd_dm_findf(test_gmap_get_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);
    if(values != NULL) {
        amxc_var_copy(&params, values);
    }
    set_args_create_device(&args, key, discovery_source,
                           tags, persistent, default_name, &params);

    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "createDevice",
                                                 &args,
                                                 &ret),
                     expected_status);

    when_failed(expected_status, exit, );

    char* validkey = gmap_create_valid_object_name(key);

    assert_true(amxc_var_get_bool(&ret));
    amxd_object_t* instance = amxd_object_get_instance(amxd_object_get_child(devices, "Device"),
                                                       validkey,
                                                       0);
    assert_non_null(instance);
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), amxd_status_ok);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(amxc_var_constcast(cstring_t, VARIANT_GET(&params, "Key")),
                        key);
    assert_string_equal(amxc_var_constcast(cstring_t, VARIANT_GET(&params, "DiscoverySource")),
                        discovery_source);
    assert_string_equal(amxc_var_constcast(cstring_t,
                                           amxc_var_get_key(&params,
                                                            "Tags",
                                                            AMXC_VAR_FLAG_DEFAULT)),
                        tags);

    free(validkey);
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void test_rpc_can_create_device_first(GMAPS_UNUSED void** state) {
    util_create_device("testKey", "testDiscoverySource", "", false, "testDefaultName", amxd_status_ok, NULL);
}

void test_rpc_can_create_device_second(GMAPS_UNUSED void** state) {
    util_create_device("test2Key", "test2DiscoverySource", "wifi", false, "test2DefaultName", amxd_status_ok, NULL);

}

void test_rpc_can_create_device_third(GMAPS_UNUSED void** state) {
    util_create_device("test3Key", "test3DiscoverySource",
                       "test3Tag1 eth test3Tag2 wifi", false, "test3DefaultName", amxd_status_ok, NULL);
}

void test_rpc_can_create_device_extra(GMAPS_UNUSED void** state) {
    amxc_var_t params;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "Master", "test2Key");
    util_create_device("00:11:13:0A:0B:10", "TEST", "lan edev mac physical eth ipv4 ipv6", false, "PC-01", amxd_status_ok, &params);


    amxc_var_clean(&params);

}

void test_rpc_can_create_device_temp(GMAPS_UNUSED void** state) {
    util_create_device("tempKey", "tempDiscoverySource",
                       "tempTag1 tempTag2 wifi", false, "tempDefaultName", amxd_status_ok, NULL);
}

void test_internal_create_device_input_validation(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(test_gmap_get_dm(), "Devices");
    amxc_var_t params;

    assert_ptr_not_equal(devices, NULL);
    amxc_var_init(&params);

    assert_int_equal(gmaps_new_device(NULL,
                                      "testKey",
                                      "testDiscoverySource",
                                      "testTag",
                                      true,
                                      "testDefaultName",
                                      &params),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_new_device(devices,
                                      NULL,
                                      "testDiscoverySource",
                                      "testTag",
                                      true,
                                      "testDefaultName",
                                      &params),
                     gmap_status_invalid_key);
    assert_int_equal(gmaps_new_device(devices,
                                      "",
                                      "testDiscoverySource",
                                      "testTag",
                                      true,
                                      "testDefaultName",
                                      &params),
                     gmap_status_invalid_key);

    amxc_var_clean(&params);
}

void test_rpc_can_destroy_device(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(test_gmap_get_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", "tempKey");
    assert_int_equal(amxd_object_invoke_function(devices, "destroyDevice", &args, &ret), amxd_status_ok);
    assert_true(amxc_var_get_bool(&ret));

    amxd_object_t* instance = amxd_object_get_instance(amxd_object_get_child(devices, "Device"),
                                                       "tempKey",
                                                       0);
    assert_null(instance);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_internal_destroy_device_input_validation(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(test_gmap_get_dm(), "Devices");
    amxc_var_t params;

    assert_ptr_not_equal(devices, NULL);
    amxc_var_init(&params);

    assert_int_equal(gmaps_delete_device(NULL,
                                         "testKey"),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_delete_device(devices,
                                         NULL),
                     gmap_status_invalid_key);
    assert_int_equal(gmaps_delete_device(devices,
                                         ""),
                     gmap_status_invalid_key);

    amxc_var_clean(&params);
}