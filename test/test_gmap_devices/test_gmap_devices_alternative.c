/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_dm_device.h"
#include "gmaps_devices_alternative.h"

void test_gmap_can_alternative_wrong_parameters(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device("test2Key");
    amxd_object_t* device_alt = gmaps_get_device("test3Key");


    gmaps_device_set_alternative(NULL, NULL);
    gmaps_device_set_alternative(device, NULL);

    gmaps_device_remove_alternative(NULL, NULL);
    gmaps_device_remove_alternative(device, NULL);

    gmaps_device_remove_alternative(NULL, device_alt);
    gmaps_device_remove_alternative(device, device_alt);

    assert_false(gmaps_device_are_alternatives(NULL, device));
    assert_false(gmaps_device_are_alternatives(device, NULL));
    assert_false(gmaps_device_are_alternatives(NULL, NULL));
    assert_false(gmaps_device_are_alternatives(device, device));


}

void test_gmap_can_device_set_alternative(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device("test2Key");
    amxd_object_t* device_alt = gmaps_get_device("test3Key");

    gmaps_device_set_alternative(device, device_alt);

    assert_true(gmaps_device_are_alternatives(device, device_alt));
    assert_true(gmaps_device_is_alternative(device_alt));

}

void test_gmap_can_invoke_set_alternative(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device("test2Key");
    amxd_object_t* device_alt = gmaps_get_device("test3Key");
    amxc_var_t args;
    amxc_var_t check_args;
    amxc_var_t result;
    amxc_var_t check_result;

    amxc_var_init(&args);
    amxc_var_init(&check_args);
    amxc_var_init(&result);
    amxc_var_init(&check_result);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&check_args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "alternative", "test3Key");
    amxc_var_add_key(cstring_t, &check_args, "alternative", "test3Key");


    amxd_object_invoke_function(device, "setAlternative", &args, &result);

    assert_int_equal(amxd_object_invoke_function(device, "isAlternative", &check_args, &check_result), amxd_status_ok);

    assert_true(amxc_var_get_bool(&check_result));

    assert_true(gmaps_device_are_alternatives(device_alt, device));
    assert_true(gmaps_device_is_alternative(device_alt));
    assert_true(gmaps_device_is_alternative_from(device, device_alt));

    assert_true(gmaps_device_is_linked_to(device, device_alt, gmap_traverse_down));

    amxc_var_clean(&args);
    amxc_var_clean(&result);
    amxc_var_clean(&check_result);
    amxc_var_clean(&check_args);

}

void test_gmap_can_device_remove_alternative(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device("test2Key");
    amxd_object_t* device_alt = gmaps_get_device("test3Key");

    gmaps_device_remove_alternative(device, device_alt);

    assert_false(gmaps_device_are_alternatives(device, device_alt));
    assert_false(gmaps_device_is_alternative(device_alt));
    assert_false(gmaps_device_is_alternative_from(device, device_alt));

}

void test_gmap_can_invoke_remove_alternatives(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device("test2Key");
    amxd_object_t* device_alt = gmaps_get_device("test3Key");

    amxc_var_t args;
    amxc_var_t check_args;
    amxc_var_t result;
    amxc_var_t check_result;


    amxc_var_init(&args);
    amxc_var_init(&check_args);
    amxc_var_init(&result);
    amxc_var_init(&check_result);


    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&check_result, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "alternative", "test3Key");
    amxc_var_add_key(cstring_t, &check_args, "alternative", "test3Key");


    amxd_object_invoke_function(device, "removeAlternative", &args, &result);
    amxd_object_invoke_function(device, "isAlternative", &check_args, &check_result);

    assert_false(amxc_var_constcast(bool, &check_result));

    assert_false(gmaps_device_are_alternatives(device_alt, device));
    assert_false(gmaps_device_is_alternative(device_alt));

    amxc_var_clean(&args);
    amxc_var_clean(&result);
    amxc_var_clean(&check_result);
    amxc_var_clean(&check_args);




}