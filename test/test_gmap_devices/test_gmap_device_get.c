/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_dm_device.h"
#include "gmaps_device_get.h"


static void test_gmap_can_device_get_flags(uint32_t flags) {
    amxc_var_t devinfo;
    amxc_var_init(&devinfo);

    assert_int_equal(gmaps_device_get("test2Key", &devinfo, flags), gmap_status_ok);

    amxc_var_clean(&devinfo);
}


void test_gmap_can_device_get(GMAPS_UNUSED void** state) {

    amxc_var_t devinfo;
    amxc_var_init(&devinfo);

    util_link_devices("test2Key", "test3Key", true);

    // Test invalid paraameters
    assert_int_equal(gmaps_device_get("", &devinfo, 0), gmap_status_invalid_key);
    assert_int_equal(gmaps_device_get(NULL, &devinfo, 0), gmap_status_invalid_key);

    // Test invalid device
    assert_int_equal(gmaps_device_get("testNonExistingKey", &devinfo, 0), gmap_status_device_not_found);

    // Now test the correct test device
    assert_int_equal(gmaps_device_get("test2Key", &devinfo, 0), gmap_status_ok);

    // Cleanup
    amxc_var_clean(&devinfo);


    test_gmap_can_device_get_flags(GMAP_NO_DETAILS);
    test_gmap_can_device_get_flags(GMAP_NO_ACTIONS);
    test_gmap_can_device_get_flags(GMAP_INCLUDE_TOPOLOGY);
    test_gmap_can_device_get_flags(GMAP_TOPOLOGY_INCLUDE_ALTERNATIVES);
    test_gmap_can_device_get_flags(GMAP_INCLUDE_LINKS);
    test_gmap_can_device_get_flags(GMAP_INCLUDE_FULL_LINKS);


}

void test_gmap_can_device_get_result(GMAPS_UNUSED void** state) {

    amxc_var_t devinfo;
    amxc_var_init(&devinfo);

    assert_int_equal(gmaps_device_get("test2Key", &devinfo, 0), gmap_status_ok);

    // Check that the variant type is a hashtable
    assert_int_equal(devinfo.type_id, AMXC_VAR_ID_HTABLE);

    // Check the contents of the htable
    assert_string_equal(amxc_var_constcast(cstring_t, amxc_var_get_key(&devinfo, "Id", 0)), "test2Key");

    amxc_var_clean(&devinfo);

}

/**
 *  Test if we correctly can invoke the get function
 */
void test_gmap_can_invoke_device_get(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device("test2Key");
    amxc_var_t args;
    amxc_var_t result;

    amxc_var_init(&args);
    amxc_var_init(&result);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "tag", "myTag");
    amxc_var_add_key(cstring_t, &args, "expression", "");
    amxc_var_add_key(cstring_t, &args, "traverse", "this");

    assert_non_null(device);

    amxd_object_invoke_function(device, "get", &args, &result);


    amxc_var_clean(&args);
    amxc_var_clean(&result);


}